from django.test import TestCase, RequestFactory

from selfplus.views import *
from selfplus.forms import *

import time
import json

class UserProfileModelTests(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="test",email="test@test.com",password="asdasaf")

    def test_userprofile_positive_on_negative_creation(self):
        up = UserProfile(user=self.user,points=-1)
        up.save()
        self.assertTrue(up.points >= 0)

    def test_userprofile_positive_on_default_creation(self):
        up = UserProfile(user=self.user)
        up.save()
        self.assertTrue(up.points >= 0)

    def tearDown(self):
        self.user.delete()


class IndexViewTests(TestCase):

    def setUp(self):
        self.url = reverse('index')

    # Test that index loads properly
    def test_index_ok_response(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('selfplus/index.html')

    # Test that forms on this page rendered properly
    def test_index_view_forms_rendered(self):
        response = self.client.get(self.url)
        login_form = response.context['login_form']
        register_form = response.context['register_form']
        self.assertIsInstance(login_form,LoginForm)
        self.assertIsInstance(register_form,RegisterForm)

class UserLoginViewTests(TestCase):

    def setUp(self):
        self.url = reverse('user_login')

    # Test that user login loads properly
    def test_user_login_ok_response(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    # Test user login response OK on post without data
    def test_user_login_ok_response_post_no_data(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, 200)

    # Test user login response OK on post with data
    def test_user_login_ok_response_post_with_data(self):
        response = self.client.post(self.url,{'username':'john','password':'smith'})
        self.assertEqual(response.status_code, 200)

    # Test that login form rendered properly
    def test_user_forms_rendered(self):
        response = self.client.get(self.url)
        login_form = response.context['login_form']
        self.assertIsInstance(login_form,LoginForm)

    # Test that invalid post data does not login
    def test_user_login_invalid_not_authenticated(self):
        response = self.client.post(self.url,{'username':'john','password':'smith'})
        self.assertRaises(forms.ValidationError)
        self.assertNotIn('_auth_user_id', self.client.session)

    # Test that valid login is authenticated
    def test_user_login_authenticated(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'
        u = User.objects.create_user(username=username,password=password)
        UserProfile.objects.create(user=u)

        # Post this data
        response = self.client.post(self.url,{'username':username, 'password':password})
        self.assertIn('_auth_user_id', self.client.session)

    # Test that valid login has redirected to setup(new user)
    def test_user_login_redirects_no_setup(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'
        u = User.objects.create_user(username=username,password=password)
        UserProfile.objects.create(user=u)

        # Post this data
        response = self.client.post(self.url, {'username':username, 'password':password})
        self.assertRedirects(response, reverse('setup_profile'), fetch_redirect_response=False)

    # Test that valid login has redirected to dailies page
    def test_user_login_redirects_setup(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'
        u = User.objects.create_user(username=username,password=password)
        UserProfile.objects.create(user=u,is_setup=True)

        # Post this data
        response = self.client.post(self.url,{'username':username, 'password':password})
        self.assertRedirects(response, reverse('user_page'),fetch_redirect_response=False)

class RegisterViewTests(TestCase):

    def setUp(self):
        self.url = reverse('register')

    # Test that register redirects to index if not a post
    def test_register_not_post_redirects(self):
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # Test that register fails with incomplete input
    # Complete tests on unit forms
    def test_register_fail_incomplete_input(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        response = self.client.post(self.url,{'username':username, 'password':password})
        # Raises validation error
        self.assertRaises(forms.ValidationError)

    # Test that register fails with invalid input
    # Complete tests on unit forms
    def test_register_fail_invalid_input(self):
        # Create dummy data
        username = 'john'
        password = 's'
        password2 = 's2'
        email = 'smh'
        email2 = 'smhah'

        response = self.client.post(self.url, {'username': username,
                                               'password': password,
                                               'password2': password2,
                                               'email': email,
                                               'email2': email2})
        # Raises validation error
        self.assertRaises(forms.ValidationError)

    # Test that register creates an account with valid input
    def test_register_ok_create_account(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'
        password2 = 'smith123'
        email = 'john@smith.com'
        email2 = 'john@smith.com'

        response = self.client.post(self.url, {'username': username,
                                               'password': password,
                                               'password2': password2,
                                               'email': email,
                                               'email2': email2})

        user = User.objects.get(username=username)
        up = UserProfile.objects.get(user=user)
        self.assertTrue(user)
        self.assertTrue(up)

    # Test that register redirects with valid input
    def test_register_ok_redirect(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'
        password2 = 'smith123'
        email = 'john@smith.com'
        email2 = 'john@smith.com'

        response = self.client.post(self.url, {'username': username,
                                               'password': password,
                                               'password2': password2,
                                               'email': email,
                                               'email2': email2})

        self.assertRedirects(response,reverse('setup_profile'),fetch_redirect_response=False)

class SetUpProfileTests(TestCase):

    def setUp(self):
        self.url = reverse('setup_profile')

        # Create dummy data
        username = 'john'
        password = 'smith123'

        # user and profile
        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        self.client.login(username=username,password=password)

    # Test that user can't see this page unless they are logged in
    def test_setup_profile_redirects_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # Test that this page displays properly after login
    def test_setup_profile_ok_logged_in(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    # Test that the forms on this page displays properly
    def test_index_view_forms_rendered(self):
        response = self.client.get(self.url)
        daily_form = response.context['daily_form']
        goal_form = response.context['goal_form']
        self.assertIsInstance(daily_form,DailyForm)
        self.assertIsInstance(goal_form,GoalForm)

    # Tests that the user is redirected to the dailies page if he completed setup
    def test_setup_profile_post_ok(self):
        goal = 'This is my goal'
        action1 = 'action1'
        action2 = 'action2'
        action3 = 'action3'

        response = self.client.post(self.url, {'goal':goal,
                                               'action1':action1,
                                               'action2':action2,
                                               'action3':action3})

        self.assertRedirects(response,reverse('user_page'))

        up = UserProfile.objects.get(user=self.user)
        self.assertTrue(up.is_setup)
        self.assertEqual(up.goal,goal)

        dailies = Daily.objects.filter(user=self.user).order_by('-points')
        self.assertEqual(dailies[0].action,action1)
        self.assertEqual(dailies[1].action,action2)
        self.assertEqual(dailies[2].action,action3)

    # Tests that the user is redirected to the dailies page if he completed setup
    def test_setup_profile_redirects_setup_complete(self):
        self.up.is_setup = True
        self.up.save()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('user_page'))

class UserProfileTests(TestCase):

    def setUp(self):
        self.url = reverse('user_profile')

        # Create dummy data
        username = 'john'
        password = 'smith123'

        goal = 'This is my goal'
        action1 = 'action1'
        action2 = 'action2'
        action3 = 'action3'
        desc = 'my description'

        # user and profile 1
        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user,goal=goal,description=desc)
        self.daily1 = Daily.objects.create(user=self.user,action=action1,points=15)
        self.daily2 = Daily.objects.create(user=self.user,action=action2,points=10)
        self.daily3 = Daily.objects.create(user=self.user,action=action3,points=5)

        self.client.login(username=username,password=password)

    # tests that we can't access this page if we are not logged in
    def test_setup_profile_redirects_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # tests the user profile page
    def test_user_profile_ok_response(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('selfplus/userprofile.html')

    # test information displayed: user profile
    def test_user_profile_context_ok_profile(self):
        response = self.client.get(self.url)
        up = response.context['userprofile']

        # Check html for username
        self.assertContains(response, up.user.username)

    # test information displayed: dailies
    def test_user_profile_context_ok_dailies(self):
        response = self.client.get(self.url)

        action1 = self.daily1.action
        action2 = self.daily2.action
        action3 = self.daily3.action

        # Check html
        self.assertContains(response, action1)
        self.assertContains(response, action2)
        self.assertContains(response, action3)

    # Test information displayed: messages
    def test_user_profile_context_ok_messages(self):
        # create a user 2 who sends messages
        username2 = 'mary'
        password2 = 'sue123'
        user2 = User.objects.create_user(username=username2,password=password2)
        UserProfile.objects.create(user=user2)

        # Create 10 messages to check pagination as well
        i = 0
        messages = []
        length = 10
        print "Requiring some 10 seconds to create items 1 second apart"
        while i<length:
            messages.append(Message.objects.create(text='message' + str(i),
                                                   sender=user2,
                                                   receiver=self.user))
            print "counting..." + str(i)
            time.sleep(1)
            i += 1

        response = self.client.get(self.url)

        # Check that the page contains the last n messages
        n = 5
        i = 1
        while i<n :
            self.assertContains(response,messages[length-i].text)
            i += 1

        response = self.client.get(self.url,{'page':2})

        # Check that the page contains the starting messages in page 2
        i = 0
        while i<n :
            self.assertContains(response,messages[i].text)
            i += 1

class AddDescTests(TestCase):

    def setUp(self):
        self.url = reverse('add_desc')

        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        self.client.login(username=username,password=password)

    # tests that we can't access this view if we are not logged in
    def test_add_desc_redirects_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # if not a post, we are redirected
    def test_add_desc_redirects_not_post(self):
        response = self.client.get(self.url)
        self.assertRedirects(response, reverse('user_profile'),fetch_redirect_response=False)

    # test that add desc changes the user's description
    def test_add_desc_post_ok(self):
        up = UserProfile.objects.get(user=self.user)

        # Assert a blank goal
        self.assertEqual(up.description, '')

        desc = 'Test Description'
        self.client.post(self.url,{'description':desc})

        up = UserProfile.objects.get(user=self.user)
        self.assertEqual(up.description, desc)

class UserPageTests(TestCase):

    def setUp(self):
        self.url = reverse('user_page')

        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        self.client.login(username=username,password=password)

    # Test that user can't see this page unless they are logged in
    def test_user_page_redirects_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # Test that this page displays properly after login
    def test_user_page_ok_logged_in(self):
        response = self.client.get(self.url)
        self.assertTemplateUsed('selfplus/userpage.html')
        self.assertEqual(response.context['username'], self.user.username)
        self.assertEqual(response.context['userprofile'], self.up)
        self.assertEqual(response.status_code, 200)

    # Test that the forms on this page displays properly
    def test_user_page_forms_rendered(self):
        response = self.client.get(self.url)
        daily_form = response.context['daily_form']
        goal_form = response.context['goal_form']
        self.assertIsInstance(daily_form,DailyForm)
        self.assertIsInstance(goal_form,GoalForm)

    def test_user_page_dailies(self):
        action1 = 'action1'
        action2 = 'action2'
        action3 = 'action3'

        daily1 = Daily.objects.create(user=self.user,action=action1,points=15)
        daily2 = Daily.objects.create(user=self.user,action=action2,points=10)
        daily3 = Daily.objects.create(user=self.user,action=action3,points=5)

        response = self.client.get(self.url)
        self.assertEqual(response.context['daily1'], daily1.action)
        self.assertEqual(response.context['daily2'], daily2.action)
        self.assertEqual(response.context['daily3'], daily3.action)

        self.assertContains(response, daily1.action)
        self.assertContains(response, daily2.action)
        self.assertContains(response, daily3.action)

    def test_user_page_dailies_complete(self):
        action1 = 'action1'
        action2 = 'action2'
        action3 = 'action3'

        daily1 = Daily.objects.create(user=self.user,action=action1,points=15)
        daily2 = Daily.objects.create(user=self.user,action=action2,points=10)
        daily3 = Daily.objects.create(user=self.user,action=action3,points=5)

        DailyLog.objects.create(user=self.user, daily=daily1, completed=datetime.datetime.now())
        DailyLog.objects.create(user=self.user, daily=daily2, completed=datetime.datetime.now())
        DailyLog.objects.create(user=self.user, daily=daily3, completed=datetime.datetime.now())

        response = self.client.get(self.url)
        context = response.context
        self.assertIsNotNone(context['daily1_complete'])
        self.assertIsNotNone(context['daily2_complete'])
        self.assertIsNotNone(context['daily3_complete'])

class DailiesCompleteTests(TestCase):

    def setUp(self):
        self.url = reverse('dailies_complete')

        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        action1 = 'action1'
        action2 = 'action2'
        action3 = 'action3'

        self.daily1 = Daily.objects.create(user=self.user,action=action1,points=15)
        self.daily2 = Daily.objects.create(user=self.user,action=action2,points=10)
        self.daily3 = Daily.objects.create(user=self.user,action=action3,points=5)

        self.client.login(username=username, password=password)

    # Test that user can't see this page unless they are logged in
    def test_dailies_complete_redirects_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # Test proper daily complete
    def test_dailies_complete_ok(self):
        response = self.client.get(self.url, {'daily_id': '1'})
        dlog = DailyLog.objects.get(daily=self.daily1)
        self.assertEqual(dlog.daily,self.daily1)
        self.assertRedirects(response,reverse('user_page'))

        response = self.client.get(self.url, {'daily_id': '2'})
        dlog = DailyLog.objects.get(daily=self.daily2)
        self.assertEqual(dlog.daily,self.daily2)
        self.assertRedirects(response,reverse('user_page'))

        response = self.client.get(self.url, {'daily_id': '3'})
        dlog = DailyLog.objects.get(daily=self.daily3)
        self.assertEqual(dlog.daily,self.daily3)
        self.assertRedirects(response,reverse('user_page'))

    # Test bad daily complete get
    def test_dailies_complete_bad_data(self):
        response = self.client.get(self.url, {'this_key_does_not_exist': 'xoxoxo'})

        dlog = DailyLog.objects.filter(user=self.user)
        self.assertEqual(len(dlog),0)
        self.assertRedirects(response,reverse('user_page'))

    # Test improper daily complete data
    def test_dailies_complete_bad_id(self):
        response = self.client.get(self.url, {'daily_id': '9999'})

        dlog = DailyLog.objects.filter(user=self.user)
        self.assertEqual(len(dlog),0)
        self.assertRedirects(response,reverse('user_page'))

    # Test empty daily complete data
    def test_dailies_complete_no_data(self):
        response = self.client.get(self.url)

        dlog = DailyLog.objects.filter(user=self.user)
        self.assertEqual(len(dlog),0)
        self.assertRedirects(response,reverse('user_page'))

class AddDailyTests(TestCase):

    def setUp(self):
        self.url = reverse('add_daily')

        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        self.client.login(username=username,password=password)

    # Test that user can't see this page unless they are logged in
    def test_add_daily_redirects_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # Test redirection when not a post
    def test_add_daily_redirects_not_post(self):
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('user_page'),fetch_redirect_response=False)

    # Test adding daily ok
    def test_add_daily_ok(self):
        action1 = 'action1'
        action2 = 'action2'
        action3 = 'action3'
        response = self.client.post(self.url, {'action1': action1, 'action2': action2, 'action3': action3})

        daily1 = Daily.objects.get(action=action1)
        self.assertEqual(daily1.action, action1)
        daily2 = Daily.objects.get(action=action2)
        self.assertEqual(daily2.action, action2)
        daily3 = Daily.objects.get(action=action3)
        self.assertEqual(daily3.action, action3)
        self.assertRedirects(response,reverse('user_page'),fetch_redirect_response=False)

    # Test adding blank daily ok
    def test_add_daily_blank_ok(self):
        response = self.client.post(self.url)

        dailies = Daily.objects.filter(user=self.user)
        self.assertEqual(len(dailies),0)
        self.assertRedirects(response,reverse('user_page'),fetch_redirect_response=False)

    # Ensure that dailies are added according to form order
    def test_add_daily_ordering(self):
        action3 = 'action3'
        # Blank action 1 and action 2
        response = self.client.post(self.url, {'action3': action3 })

        # This means action 3 moves up
        daily1 = Daily.objects.get(action=action3)
        self.assertEqual(daily1.action, action3)
        self.assertEqual(daily1.points, 15)
        self.assertRedirects(response,reverse('user_page'),fetch_redirect_response=False)

    # Test replacing all dailies
    def test_add_daily_full_replace(self):
        action1 = 'initial action1'
        action2 = 'initial action2'
        action3 = 'initial action3'

        action1_2 = 'edited action1'
        action2_2 = 'edited action2'
        action3_2 = 'edited action3'

        daily1 = Daily.objects.create(user=self.user,action=action1,points=15)
        daily2 = Daily.objects.create(user=self.user,action=action2,points=10)
        daily3 = Daily.objects.create(user=self.user,action=action3,points=5)

        # Edit the actions
        response = self.client.post(self.url, {'action1': action1_2, 'action2':action2_2, 'action3': action3_2})

        # test that dailies1-2-3 has been deactiviated
        daily1 = Daily.objects.get(action=action1)
        self.assertFalse(daily1.active)
        daily2 = Daily.objects.get(action=action2)
        self.assertFalse(daily2.active)
        daily3 = Daily.objects.get(action=action3)
        self.assertFalse(daily3.active)

        # Test that the new dailies exist
        daily1 = Daily.objects.get(action=action1_2)
        self.assertEqual(daily1.action, action1_2)
        daily2 = Daily.objects.get(action=action2_2)
        self.assertEqual(daily2.action, action2_2)
        daily3 = Daily.objects.get(action=action3_2)
        self.assertEqual(daily3.action, action3_2)
        self.assertRedirects(response,reverse('user_page'),fetch_redirect_response=False)

    # Test replacing partial dailies
    def test_add_daily_partial_replace(self):
        action1 = 'initial action1'
        action2 = 'initial action2'
        action3 = 'initial action3'

        action3_2 = 'edited action3'

        Daily.objects.create(user=self.user,action=action1,points=15)
        Daily.objects.create(user=self.user,action=action2,points=10)
        Daily.objects.create(user=self.user,action=action3,points=5)

        # Edit the actions
        response = self.client.post(self.url, {'action3': action3_2})

        # test that daily 3 has been deactiviated
        daily3 = Daily.objects.get(action=action3)
        self.assertFalse(daily3.active)

        # Test that daily 1/2 is the same but daily 3 has been replaced
        daily1 = Daily.objects.get(action=action1)
        self.assertEqual(daily1.action, action1)
        daily2 = Daily.objects.get(action=action2)
        self.assertEqual(daily2.action, action2)
        daily3 = Daily.objects.get(action=action3_2)
        self.assertEqual(daily3.action, action3_2)
        self.assertRedirects(response,reverse('user_page'),fetch_redirect_response=False)

    # Test erasing dailies
    def test_add_daily_remove(self):
        action1 = 'initial action1'
        action2 = 'initial action2'
        action3 = 'initial action3'

        action3_2 = 'final action3'

        Daily.objects.create(user=self.user,action=action1,points=15)
        Daily.objects.create(user=self.user,action=action2,points=10)
        Daily.objects.create(user=self.user,action=action3,points=5)

        # a blank action1  and 2 means a removal
        response = self.client.post(self.url, {'action1':'', 'action2':'', 'action3': action3_2})

        # test that dailies1-2 has been deactiviated
        daily1 = Daily.objects.get(action=action1)
        self.assertFalse(daily1.active)
        daily2 = Daily.objects.get(action=action2)
        self.assertFalse(daily2.active)

        # Test that daily 3 has become the priority 1 item
        daily3 = Daily.objects.get(action=action3_2)
        self.assertEqual(daily3.action, action3_2)
        self.assertEqual(daily3.points, 15)
        self.assertRedirects(response,reverse('user_page'),fetch_redirect_response=False)

    # Test duplicate replace
    def test_add_daily_duplicate(self):
        action1 = 'initial action1'

        daily = Daily.objects.create(user=self.user,action=action1,points=15)

        # Here we try to create the same action
        response = self.client.post(self.url, {'action1':action1})

        # Make sure the item was not duplicated
        daily1 = Daily.objects.filter(action=action1)
        self.assertEqual(len(daily1),1)
        daily1 = Daily.objects.get(action=action1)
        self.assertTrue(daily1.active)
        self.assertRedirects(response,reverse('user_page'),fetch_redirect_response=False)


class AddGoalTests(TestCase):

    def setUp(self):
        self.url = reverse('add_goal')

        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        self.client.login(username=username,password=password)

    # tests that we can't access this view if we are not logged in
    def test_add_goal_redirects_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response, reverse('index'))

    # if not a post, we are redirected
    def test_add_goal_redirects_not_post(self):
        response = self.client.get(self.url)
        self.assertRedirects(response, reverse('user_page'),fetch_redirect_response=False)

    # test that add goal changes the user's goal
    def test_add_goal_post_ok(self):
        up = UserProfile.objects.get(user=self.user)

        # Assert a blank goal
        self.assertEqual(up.goal,"")

        goal = "Test Goal"
        self.client.post(self.url,{'goal':goal})

        up = UserProfile.objects.get(user=self.user)
        self.assertEqual(up.goal, goal)

class CommunityTests(TestCase):

    def setUp(self):
        self.url = reverse('community')

        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        self.client.login(username=username,password=password)

    # tests that we can't access this view if we are not logged in
    def test_community_redirects_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # Test that page loads properly
    def test_community_ok_response(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('selfplus/community.html')

class CommunitySearchTests(TestCase):

    def setUp(self):
        self.url = reverse('community_search')

        # Create dummy data
        username = 'john'
        password = 'smith123'
        action1 = 'action1'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)
        self.daily1 = Daily.objects.create(user=self.user, action=action1,points=15)

        self.client.login(username=username,password=password)

    # tests that we can't access this view if we are not logged in
    def test_community_search_redirects_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response, reverse('index'))

    # test that an empty string does nothing
    def test_community_search_empty_string_fails(self):
        response = self.client.get(self.url,{'str':""})
        self.assertEqual(response.content, '')

    # test that a private user does not appear in search
    def test_community_search_ok(self):
        response = self.client.get(self.url,{'str':'john'})
        self.assertContains(response, 'Selfplus could not find relevant results ')

    # test a search of a public profile
    def test_community_search_name_ok(self):
        self.up.is_private = False
        self.up.save()
        username = self.user.username
        response = self.client.get(self.url,{'str': username})
        self.assertTrue(response.context['search_return'])
        self.assertContains(response, 'Usernames relevant to keyword : \"' + username + '\" ')

    # test a search of dailies
    def test_community_search_daily_ok(self):
        self.up.is_private = False
        self.up.save()
        daily = 'action1'
        response = self.client.get(self.url,{'str':daily})
        self.assertTrue(response.context['search_return'])
        self.assertContains(response, 'Users with dailies relevant to keyword : \"' + daily + '\" ')

class CommunityUserTests(TestCase):

    def setUp(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username, password=password)
        self.up = UserProfile.objects.create(user=self.user)

        username2 = 'mary'
        password2 = 'sue123'

        self.user2 = User.objects.create_user(username=username2,password=password2)
        self.up2 = UserProfile.objects.create(user=self.user2)

        self.client.login(username=username, password=password)

    # tests that we can't access this view if we are not logged in
    def test_community_user_redirects_not_logged_in(self):
        self.client.logout()
        self.url = reverse('community_user', args=['x'])
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # tests that the page gets redirected to the community page if the user does not exist
    def test_community_user_redirects_no_user(self):
        self.url = reverse('community_user', args=['x'])
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('community'),fetch_redirect_response=False)

    # tests that the page gets redirected to the user's profile if the request is his username
    def test_community_user_redirects_is_owner(self):
        self.url = reverse('community_user', args=[self.user.username])
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('user_profile'),fetch_redirect_response=False)

    # tests that the page gets redirected to community page if requested username is private
    def test_community_user_redirects_is_private(self):
        self.url = reverse('community_user', args=[self.user2.username])
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('community'),fetch_redirect_response=False)

    # tests that the page successfully loads with a proper public username
    def test_community_user_ok(self):
        self.up2.is_private = False
        self.up2.save()
        self.url = reverse('community_user', args=[self.user2.username])
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('selfplus/userprofile.html')
        self.assertContains(response,'You are browsing ' + self.user2.username + '\'s page')

class AddMessageTests(TestCase):

    def setUp(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        username2 = 'mary'
        password2 = 'sue123'

        self.user2 = User.objects.create_user(username=username2, password=password2)
        self.up2 = UserProfile.objects.create(user=self.user2)

        self.url = reverse('add_message', args=[self.user2.username])
        self.client.login(username=username, password=password)

    # tests that we can't access this view if we are not logged in
    def test_add_message_redirects_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response, reverse('index'))

    def test_add_message_not_post_redirects(self):
        response = self.client.get(self.url)
        self.assertRedirects(response, reverse('community_user', args=[self.user2.username]), fetch_redirect_response=False)

    def test_add_message_ok(self):
        message = 'test message'
        response = self.client.post(self.url, {'message': message})

        self.assertRedirects(response, reverse('community_user', args=[self.user2.username]),fetch_redirect_response=False)

        msg = Message.objects.get(receiver=self.user2,sender=self.user)
        self.assertIsNotNone(msg)
        self.assertTrue(msg.text == message)

class AddFriendTests(TestCase):

    def setUp(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        username2 = 'mary'
        password2 = 'sue123'

        self.user2 = User.objects.create_user(username=username2,password=password2)
        self.up2 = UserProfile.objects.create(user=self.user2)

        self.client.login(username=username, password=password)

    # tests that we can't access this view if we are not logged in
    def test_add_friend_redirects_not_logged_in(self):
        self.client.logout()
        self.url = reverse('add_friend', args=[self.user2.username])
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # test sending an invite
    def test_add_friend_new_request(self):
        self.url = reverse('add_friend', args=[self.user2.username])
        response = self.client.get(self.url)

        rel = Relation.objects.get(primary_user=self.user)
        self.assertIsNotNone(rel)
        self.assertEqual(rel.secondary_user.username, self.user2.username)
        self.assertEqual(rel.status, RelStatus.REQUESTED)

        # Make sure that the page is successfully redirected
        self.assertRedirects(response, reverse('community_user', args=[self.user2.username]), fetch_redirect_response=False)

    # test sending an invite to someone who invited the user already
    def test_add_friend_receiver_sent_request(self):
        Relation.objects.create(primary_user=self.user2,secondary_user=self.user,status=RelStatus.REQUESTED)

        self.url = reverse('add_friend', args=[self.user2.username])
        response = self.client.get(self.url)

        # Make sure that the relationship is not doubled
        rels = Relation.objects.filter(primary_user=self.user, secondary_user=self.user2)
        self.assertEqual(len(rels), 0)

        # Make sure that the relationship status was updated
        rels = Relation.objects.get(primary_user=self.user2)
        self.assertIsNotNone(rels)
        self.assertEqual(rels.status, RelStatus.ACCEPTED)

        # Make sure that the page is successfully redirected
        self.assertRedirects(response, reverse('community_user', args=[self.user2.username]), fetch_redirect_response=False)

    # test sending a double invite
    def test_add_friend_sender_sent_request(self):
        Relation.objects.create(primary_user=self.user,secondary_user=self.user2,status=RelStatus.REQUESTED)
        self.url = reverse('add_friend', args=[self.user2.username])
        response = self.client.get(self.url)

        # Make sure that the relationship is not doubled
        rels = Relation.objects.filter(primary_user=self.user, secondary_user=self.user2)
        self.assertEqual(len(rels), 1)

        # Make sure that the page is successfully redirected
        self.assertRedirects(response, reverse('community_user', args=[self.user2.username]), fetch_redirect_response=False)

class AcceptFriendTests(TestCase):

    def setUp(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        username2 = 'mary'
        password2 = 'sue123'

        self.user2 = User.objects.create_user(username=username2,password=password2)
        self.up2 = UserProfile.objects.create(user=self.user2)

        self.client.login(username=username, password=password)

    # tests that we can't access this view if we are not logged in
    def test_accept_friend_redirects_not_logged_in(self):
        self.client.logout()
        self.url = reverse('accept_friend', args=[self.user2.username])
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # test accepting a non existent friend request is redirected
    def test_accept_friend_request_does_not_exist(self):
        self.url = reverse('accept_friend', args=[self.user2.username])
        response = self.client.get(self.url)

        # No request was made
        rels = Relation.objects.filter(primary_user=self.user, secondary_user=self.user2)
        self.assertEqual(len(rels), 0)

        # No request was made the other way around
        rels = Relation.objects.filter(primary_user=self.user2, secondary_user=self.user)
        self.assertEqual(len(rels), 0)

        # Make sure that the page is successfully redirected
        self.assertRedirects(response, reverse('user_friends'), fetch_redirect_response=False)

    # test accepting the friend request
    def test_accept_friend_request(self):
        Relation.objects.create(primary_user=self.user2,secondary_user=self.user,status=RelStatus.REQUESTED)

        self.url = reverse('accept_friend', args=[self.user2.username])
        response = self.client.get(self.url)

        # Make sure that the relationship is not doubled
        rels = Relation.objects.filter(primary_user=self.user, secondary_user=self.user2)
        self.assertEqual(len(rels), 0)

        # Make sure that the relationship status was updated
        rels = Relation.objects.get(primary_user=self.user2)
        self.assertIsNotNone(rels)
        self.assertEqual(rels.status, RelStatus.ACCEPTED)

        # Make sure that the page is successfully redirected
        self.assertRedirects(response, reverse('user_friends'), fetch_redirect_response=False)

class RejectFriendTests(TestCase):
    def setUp(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        username2 = 'mary'
        password2 = 'sue123'

        self.user2 = User.objects.create_user(username=username2,password=password2)
        self.up2 = UserProfile.objects.create(user=self.user2)

        self.client.login(username=username, password=password)

    # tests that we can't access this view if we are not logged in
    def test_reject_friend_redirects_not_logged_in(self):
        self.client.logout()
        self.url = reverse('reject_friend', args=[self.user2.username])
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # test rejecting a non existent friend request is redirected
    def test_reject_friend_request_does_not_exist(self):
        self.url = reverse('reject_friend', args=[self.user2.username])
        response = self.client.get(self.url)

        # Make sure that the page is successfully redirected
        self.assertRedirects(response, reverse('user_friends'), fetch_redirect_response=False)

    # test rejecting the friend request
    def test_reject_friend_request(self):
        Relation.objects.create(primary_user=self.user2,secondary_user=self.user,status=RelStatus.REQUESTED)

        self.url = reverse('reject_friend', args=[self.user2.username])
        response = self.client.get(self.url)

        # Make sure that the relationship was removed
        rels = Relation.objects.filter(primary_user=self.user2, secondary_user=self.user)
        self.assertEqual(len(rels), 0)

        # Make sure that the page is successfully redirected
        self.assertRedirects(response, reverse('user_friends'), fetch_redirect_response=False)

class CancelRequestTests(TestCase):
    def setUp(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        username2 = 'mary'
        password2 = 'sue123'

        self.user2 = User.objects.create_user(username=username2,password=password2)
        self.up2 = UserProfile.objects.create(user=self.user2)

        self.client.login(username=username, password=password)

    # tests that we can't access this view if we are not logged in
    def test_cancel_request_redirects_not_logged_in(self):
        self.client.logout()
        self.url = reverse('cancel_request', args=[self.user2.username])
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # test cancelling a non existent friend request gracefully redirects
    def test_cancel_request_does_not_exist(self):
        self.url = reverse('cancel_request', args=[self.user2.username])
        response = self.client.get(self.url)

        # Make sure that the page is successfully redirected
        self.assertRedirects(response, reverse('user_friends'), fetch_redirect_response=False)

    # test cancelling a friend request
    def test_reject_friend_request(self):
        Relation.objects.create(primary_user=self.user,secondary_user=self.user2,status=RelStatus.REQUESTED)

        self.url = reverse('cancel_request', args=[self.user2.username])
        response = self.client.get(self.url)

        # Make sure that the relationship was removed
        rels = Relation.objects.filter(primary_user=self.user, secondary_user=self.user2)
        self.assertEqual(len(rels), 0)

        # Make sure that the page is successfully redirected
        self.assertRedirects(response, reverse('user_friends'), fetch_redirect_response=False)

class UserProgressTests(TestCase):

    def setUp(self):
        self.url = reverse('user_progress')

        # Create dummy data
        username = 'john'
        password = 'smith123'
        action1 = 'action1'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)
        self.daily1 = Daily.objects.create(user=self.user,action=action1,points=15)

        self.daily_date = datetime.datetime(year=2015,month=1, day=1)
        DailyLog.objects.create(user=self.user, daily=self.daily1, completed=self.daily_date)

        self.client.login(username=username,password=password)

    # tests that we can't access this view if we are not logged in
    def test_user_progress_redirects_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # tests that this page loads ok
    def test_user_progress_ok(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('selfplus/progress.html')

    # tests that ajax calls are ok
    def test_user_progress_ajax_call_ok(self):
        year = self.daily_date.year
        month = self.daily_date.month

        response = self.client.get(self.url, {'year': year,'month': month}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        json_string = response.content
        data = json.loads(json_string)

        self.assertEqual(data['year_points'][0], self.daily1.points)
        self.assertEqual(data['month_points'][0], self.daily1.points)

class UserFriendsTests(TestCase):

    def setUp(self):
        self.url = reverse('user_friends')

        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        username2 = 'mary'
        password2 = 'sue123'

        self.user2 = User.objects.create_user(username=username2,password=password2)
        self.up2 = UserProfile.objects.create(user=self.user2)

        self.client.login(username=username,password=password)

    # tests that we can't access this view if we are not logged in
    def test_user_friends_redirects_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # tests that this page loads ok
    def test_user_friends_ok(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('selfplus/friends.html')

    # test friendships
    def test_user_friends_friendships(self):
        # create friendships
        Relation.objects.create(primary_user=self.user, secondary_user=self.user2,status=RelStatus.ACCEPTED)
        response = self.client.get(self.url)
        self.assertEqual(response.context['friends'][0][0], self.user2)

    # test pending from user
    def test_user_friends_pending(self):
        # create friendships
        Relation.objects.create(primary_user=self.user, secondary_user=self.user2,status=RelStatus.REQUESTED)
        response = self.client.get(self.url)
        self.assertEqual(response.context['pending'][0][0], self.user2)

    # test pending requests from other users
    def test_user_friends_requests(self):
        # create friendships
        Relation.objects.create(primary_user=self.user2, secondary_user=self.user, status=RelStatus.REQUESTED)
        response = self.client.get(self.url)
        self.assertEqual(response.context['requests'][0][0], self.user2)

class UserSettingsTests(TestCase):

    def setUp(self):
        self.url = reverse('user_settings')

        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        self.client.login(username=username,password=password)

    # tests that we can't access this view if we are not logged in
    def test_user_settings_redirects_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # tests that this page loads ok
    def test_user_settings_ok(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('selfplus/settings.html')

    # test forms properly rendered
    def test_user_settings_forms_rendered(self):
        response = self.client.get(self.url)
        profile_form = response.context['profile_form']
        password_form = response.context['password_form']
        email_form = response.context['email_form']
        self.assertIsInstance(profile_form,ProfileForm)
        self.assertIsInstance(password_form,PasswordForm)
        self.assertIsInstance(email_form,EmailForm)

    # test ajax privacy on
    def test_user_settings_privacy_on(self):
        # Default is true
        self.up.is_private = False
        self.up.save()
        self.assertFalse(self.up.is_private)

        response = self.client.post(self.url, {'private_box': 'True'}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        # Test that it was changed in the database
        up = UserProfile.objects.get(user=self.user)
        self.assertTrue(up.is_private)

        # Test that it was changed in the page
        self.assertTrue(response.context['is_private'])

    # test ajax privacy off
    def test_user_settings_privacy_off(self):
        response = self.client.post(self.url, {'private_box': 'False'}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        # Test that it was changed in the database
        up = UserProfile.objects.get(user=self.user)
        self.assertFalse(up.is_private)

        # Test that it was changed in the page
        self.assertFalse(response.context['is_private'])

class EditProfileTests(TestCase):

    def setUp(self):
        self.url = reverse('edit_profile')

        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        self.client.login(username=username,password=password)

    # tests that we can't access this view if we are not logged in
    def test_edit_profile_redirects_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # tests redirect ok if request is not post
    def test_edit_profile_redirects_not_post(self):
        response = self.client.get(self.url)
        self.assertRedirects(response, reverse('user_settings'), fetch_redirect_response=False)


class ChangePasswordTests(TestCase):

    def setUp(self):
        self.url = reverse('change_password')

        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        self.client.login(username=username,password=password)

    # tests that we can't access this view if we are not logged in
    def test_change_password_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # tests redirect ok if request is not post
    def test_change_password_redirects_not_post(self):
        response = self.client.get(self.url)
        self.assertRedirects(response, reverse('user_settings'), fetch_redirect_response=False)

    # tests forms rendered
    def test_change_password_ok(self):
        old_password = 'smith123'
        password = 'smithereens'
        password2 = 'smithereens'

        response = self.client.post(self.url, {'old_password': old_password, 'password': password, 'password2': password2})
        self.assertRedirects(response, reverse('user_settings'), fetch_redirect_response=False)

        # logout and login using the new password
        self.client.logout()
        self.client.login(username=self.user.username,password=password)
        self.assertIn('_auth_user_id', self.client.session)

class ChangeEmailTests(TestCase):

    def setUp(self):
        self.url = reverse('change_email')

        # Create dummy data
        username = 'john'
        password = 'smith123'
        email = 'john@smith.com'

        self.user = User.objects.create_user(username=username,password=password,email=email)
        self.up = UserProfile.objects.create(user=self.user)

        self.client.login(username=username,password=password)

    # tests that we can't access this view if we are not logged in
    def test_change_email_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # tests redirect ok if request is not post
    def test_change_email_redirects_not_post(self):
        response = self.client.get(self.url)
        self.assertRedirects(response, reverse('user_settings'), fetch_redirect_response=False)

    # tests forms rendered
    def test_change_email_ok(self):
        email = 'smithereens@smith.com'
        email2 = 'smithereens@smith.com'

        response = self.client.post(self.url, {'email': email, 'email2': email2})
        self.assertRedirects(response, reverse('user_settings'), fetch_redirect_response=False)

        user = User.objects.get(username=self.user.username)
        self.assertEqual(email,user.email)

class UserLogoutTests(TestCase):

    def setUp(self):
        self.url = reverse('logout')

        # Create dummy data
        username = 'john'
        password = 'smith123'
        email = 'john@smith.com'

        self.user = User.objects.create_user(username=username,password=password,email=email)
        self.up = UserProfile.objects.create(user=self.user)

        self.client.login(username=username,password=password)

    # tests that we can't access this view if we are not logged in
    def test_user_logout_not_logged_in(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))

    # tests that we can't access this view if we are not logged in
    def test_user_logout_ok(self):
        response = self.client.get(self.url)
        self.assertRedirects(response,reverse('index'))
        self.assertNotIn('_auth_user_id', self.client.session)


# Helper Function TestsCase
class GetDailiesTests(TestCase):

    def setUp(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        action1 = 'initial action1'
        action2 = 'initial action2'
        action3 = 'initial action3'

        self.d1 = Daily.objects.create(user=self.user,action=action1,points=15)
        self.d2 = Daily.objects.create(user=self.user,action=action2,points=10)
        self.d3 = Daily.objects.create(user=self.user,action=action3,points=5)

        self.client.login(username=username,password=password)

    # Test that this function does not retrieve inactives
    def test_get_dailies_no_inactives(self):
        # Create inactive dailies
        inactive1 = 'inactive action1'
        inactive2 = 'inactive action2'
        inactive3 = 'inactive action3'
        d1 = Daily.objects.create(user=self.user,action=inactive1,points=15,active=False)
        d2 = Daily.objects.create(user=self.user,action=inactive2,points=10,active=False)
        d3 = Daily.objects.create(user=self.user,action=inactive3,points=5,active=False)

        dailies = []
        get_dailies(self.user, dailies)
        self.assertNotIn(d1.action,dailies)
        self.assertNotIn(d2.action,dailies)
        self.assertNotIn(d3.action,dailies)

    # Test that this function gives proper results when action_only is set to false
    def test_get_dailies_action_only_false(self):
        dailies = []
        get_dailies(self.user, dailies, action_only=False)

        expected = [self.d1,self.d2,self.d3]
        self.assertListEqual(dailies, expected)

    # Test that this function gives proper results when action_only is set to true
    def test_get_dailies_action_only_true(self):
        dailies = []
        get_dailies(self.user, dailies, action_only=True)

        expected = [self.d1.action,self.d2.action,self.d3.action]
        self.assertListEqual(dailies, expected)

    # Test that this function hands out none with an active daily search is not found
    def test_get_dailies_does_not_exist(self):
        # Let's make all dailies inactive
        self.d1.active = False
        self.d1.save()
        self.d2.active = False
        self.d2.save()
        self.d3.active = False
        self.d3.save()
        dailies = []
        get_dailies(self.user, dailies, action_only=False)

        expected = [None, None, None]
        self.assertListEqual(dailies, expected)

class GetProfileTests(TestCase):

    def setUp(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

    # test that retrieving the profile is successful
    def test_get_profile_ok(self):
        up = get_profile(self.user)
        self.assertEqual(up, self.up)

    # test that retrieving the profile when we pass an empty user brings back None
    def test_get_profile_none_fails(self):
        up = get_profile(None)
        self.assertIsNone(up)

    # test that retrieving the profile when the UP does not exist brings back None
    def test_get_profile_does_not_exist_fails(self):
        username = 'noprofilejohn'
        password = 'smith123'
        user = User.objects.create_user(username=username,password=password)

        up = get_profile(user)
        self.assertIsNone(up)

class NavbarToggleTests(TestCase):

    def setUp(self):
        self.expected_dict = {}
        self.expected_dict['profile_nav'] = 'inactive'
        self.expected_dict['dailies_nav'] = 'inactive'
        self.expected_dict['progress_nav'] = 'inactive'
        self.expected_dict['friends_nav'] = 'inactive'
        self.expected_dict['community_nav'] = 'inactive'

    def test_navbar_toggle_empty(self):
        context_dict = {}
        self.expected_dict['profile_nav'] = 'active'

        navbar_toggle(context_dict, None)
        self.assertDictEqual(context_dict,self.expected_dict)

    def test_navbar_toggle_empty_string(self):
        context_dict = {}
        self.expected_dict['profile_nav'] = 'active'

        navbar_toggle(context_dict, "")
        self.assertDictEqual(context_dict,self.expected_dict)

    def test_navbar_toggled_invalid_string(self):
        context_dict = {}
        self.expected_dict['profile_nav'] = 'active'

        navbar_toggle(context_dict, "invalid_string")
        self.assertDictEqual(context_dict,self.expected_dict)

    def test_navbar_toggle_profile(self):
        context_dict = {}
        self.expected_dict['profile_nav'] = 'active'

        navbar_toggle(context_dict, 'profile_nav')
        self.assertDictEqual(context_dict,self.expected_dict)

    def test_navbar_toggle_progress(self):
        context_dict = {}
        self.expected_dict['progress_nav'] = 'active'

        navbar_toggle(context_dict, 'progress_nav')
        self.assertDictEqual(context_dict,self.expected_dict)

    def test_navbar_toggle_dailies(self):
        context_dict = {}
        self.expected_dict['dailies_nav'] = 'active'

        navbar_toggle(context_dict, 'dailies_nav')
        self.assertDictEqual(context_dict,self.expected_dict)

    def test_navbar_toggle_friends(self):
        context_dict = {}
        self.expected_dict['friends_nav'] = 'active'

        navbar_toggle(context_dict, 'friends_nav')
        self.assertDictEqual(context_dict,self.expected_dict)

    def test_navbar_toggle_community(self):
        context_dict = {}
        self.expected_dict['community_nav'] = 'active'

        navbar_toggle(context_dict, 'community_nav')
        self.assertDictEqual(context_dict,self.expected_dict)

    def test_context_dict_not_empty_replaced(self):
        context_dict = {}
        context_dict['profile_nav'] = 'active'
        context_dict['dailies_nav'] = 'active'
        context_dict['progress_nav'] = 'active'
        context_dict['friends_nav'] = 'active'
        context_dict['community_nav'] = 'active'

        self.expected_dict['profile_nav'] = 'active'
        navbar_toggle(context_dict, 'profile_nav')
        self.assertDictEqual(context_dict,self.expected_dict)

class GetMessagesTests(TestCase):

    def setUp(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        username2 = 'mary'
        password2 = 'sue123'
        self.user2 = User.objects.create_user(username=username2,password=password2)
        self.up2 = UserProfile.objects.create(user=self.user2)

        # Create 10 messages 1 second apart
        i = 0
        messages = []
        length = 10
        print "Requiring some 10 seconds to create items 1 second apart"
        while i<length:
            messages.append(Message.objects.create(text='message' + str(i),
                                                   sender=self.user2,
                                                   receiver=self.user))
            print "counting..." + str(i)
            time.sleep(1)
            i += 1

        # Dummy request
        self.url = reverse('community_user', args=[self.user.username])
        self.factory = RequestFactory()

    def test_get_messages_ok(self):
        request = self.factory.get(self.url,{'page': '2'})

        message_list = Message.objects.filter(receiver=self.user).order_by('-date_posted')
        paginator = Paginator(message_list,5)

        expected_msgs = paginator.page(2)
        expected_range = [1,2]
        expected_current = 2

        messages, range, current = get_messages(request, self.user)
        self.assertItemsEqual(expected_msgs.object_list, messages.object_list)
        self.assertListEqual(expected_range,range)
        self.assertEqual(expected_current,current)

class FillCalendarTests(TestCase):

    def setUp(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        action1 = 'action1'
        action2 = 'action2'
        action3 = 'action3'

        daily1 = Daily.objects.create(user=self.user,action=action1,points=15)
        daily2 = Daily.objects.create(user=self.user,action=action2,points=10)
        daily3 = Daily.objects.create(user=self.user,action=action3,points=5)

        # Create a date scenario for the test
        self.month = 1
        self.year = 2000
        self.day = 1
        self.date_completed = datetime.date(year=self.year,month=self.month,day=self.day)

        self.dlog1 = DailyLog.objects.create(user=self.user, daily=daily1, completed=self.date_completed)
        self.dlog2 = DailyLog.objects.create(user=self.user, daily=daily2, completed=self.date_completed)
        self.dlog3 = DailyLog.objects.create(user=self.user, daily=daily3, completed=self.date_completed)

    def test_fill_calendar_ok(self):
        now = datetime.datetime.now()
        c = calendar.Calendar()
        c.setfirstweekday(calendar.SUNDAY)
        monthly_calendar = c.monthdatescalendar(self.year, self.month)
        dlog = DailyLog.objects.filter(user=self.user).filter(completed__range=(monthly_calendar[0][0],monthly_calendar[-1][-1]))

        full_calendar = fill_calendar(calendar=monthly_calendar, dlog=dlog)

        date_exists = False
        for week in full_calendar:
            for date, logs, points in week:
                if date == self.date_completed:
                    date_exists = True
                    # check log integrity
                    self.assertIn(self.dlog1, logs)
                    self.assertIn(self.dlog2, logs)
                    self.assertIn(self.dlog3, logs)
                    # check points build up correct
                    points1 = self.dlog1.daily.points
                    points2 = self.dlog2.daily.points
                    points3 = self.dlog3.daily.points
                    self.assertEqual(points, points1+points2+points3)

        self.assertTrue(date_exists)

class GetActiveLogsTests(TestCase):

    def setUp(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        action1 = 'action1'
        action2 = 'action2'
        action3 = 'action3'

        self.daily1 = Daily.objects.create(user=self.user,action=action1,points=15)
        self.daily2 = Daily.objects.create(user=self.user,action=action2,points=10)
        self.daily3 = Daily.objects.create(user=self.user,action=action3,points=5)

        # Create a date scenario for the test
        self.month = 1
        self.year = 2000
        self.day = 1
        self.date_completed = datetime.date(year=self.year,month=self.month,day=self.day)

        self.dlog1 = DailyLog.objects.create(user=self.user, daily=self.daily1, completed=self.date_completed)
        self.dlog2 = DailyLog.objects.create(user=self.user, daily=self.daily2, completed=self.date_completed)
        self.dlog3 = DailyLog.objects.create(user=self.user, daily=self.daily3, completed=self.date_completed)

    # test get active logs with valid input
    def test_get_active_logs_ok(self):
        dailies = [self.daily1, self.daily2, self.daily3]
        dlogs = get_active_logs(dailies, self.date_completed)
        expected = [self.dlog1, self.dlog2, self.dlog3]
        self.assertListEqual(expected, dlogs)

    # test that get active logs returns None with dailies are invalid
    def test_get_active_logs_invalid_dailies(self):
        dailies = [None, None, self.daily3]
        dlogs = get_active_logs(dailies, self.date_completed)
        expected = [None, None, self.dlog3]
        self.assertListEqual(expected, dlogs)

    # test that get active logs returns None when the daily log queried does not exist
    def test_get_active_logs_no_entry(self):
        dailies = [self.daily1, self.daily2, self.daily3]
        # look for an entry one day ahead of the date completed scenario
        next_day = datetime.date(year=self.year, month=self.month, day=self.date_completed.day+1)
        dlogs = get_active_logs(dailies,next_day)
        expected = [None, None, None]

class GetYearPointsTests(TestCase):

    def setUp(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        action1 = 'action1'
        action2 = 'action2'
        action3 = 'action3'

        self.daily1 = Daily.objects.create(user=self.user,action=action1,points=15)
        self.daily2 = Daily.objects.create(user=self.user,action=action2,points=10)
        self.daily3 = Daily.objects.create(user=self.user,action=action3,points=5)

        # Create a date scenario for the test
        self.month = 1
        self.year = 2000
        self.day = 1
        self.date_completed = datetime.date(year=self.year,month=self.month,day=self.day)

        self.dlog1 = DailyLog.objects.create(user=self.user, daily=self.daily1, completed=self.date_completed)
        self.dlog2 = DailyLog.objects.create(user=self.user, daily=self.daily2, completed=self.date_completed)
        self.dlog3 = DailyLog.objects.create(user=self.user, daily=self.daily3, completed=self.date_completed)

    # test function with valid input
    def test_get_year_points_ok(self):
        points = get_year_points(self.user, self.year)

        expected = []

        month_limit = 12
        month = 1
        total = 0
        while month <= month_limit:
            if self.month == month:
                total = self.daily1.points + self.daily2.points + self.daily3.points
            expected.append(total)
            month += 1

        self.assertEqual(expected, points)

    # test function with empty year
    def test_get_year_points_empty_year(self):
        points = get_year_points(self.user, self.year-1)

        expected = []

        month_limit = 12
        month = 1
        while month <= month_limit:
            expected.append(0)
            month += 1

        self.assertEqual(expected, points)

    # test function with empty user
    def test_get_year_points_invalid_user(self):
        points = get_year_points(None, self.year)

        expected = []

        month_limit = 12
        month = 1
        while month <= month_limit:
            expected.append(0)
            month += 1

        self.assertEqual(expected, points)

class GetMonthPointsTests(TestCase):

    def setUp(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        action1 = 'action1'
        action2 = 'action2'
        action3 = 'action3'

        self.daily1 = Daily.objects.create(user=self.user,action=action1,points=15)
        self.daily2 = Daily.objects.create(user=self.user,action=action2,points=10)
        self.daily3 = Daily.objects.create(user=self.user,action=action3,points=5)

        # Create a date scenario for the test
        self.month = 2
        # Function gets only current year months
        self.year = datetime.datetime.now().year
        self.day = 1
        self.date_completed = datetime.date(year=self.year,month=self.month,day=self.day)

        self.dlog1 = DailyLog.objects.create(user=self.user, daily=self.daily1, completed=self.date_completed)
        self.dlog2 = DailyLog.objects.create(user=self.user, daily=self.daily2, completed=self.date_completed)
        self.dlog3 = DailyLog.objects.create(user=self.user, daily=self.daily3, completed=self.date_completed)

    # test function with valid input
    def test_get_month_points_ok(self):
        points = get_month_points(self.user, self.month)

        expected = []

        # create a range of days of the month in that year
        days = calendar.monthrange(year=self.year, month=self.month)[1]
        day = 1
        total = 0
        while day <= days:
            if self.day == day:
                total = self.daily1.points + self.daily2.points + self.daily3.points
            expected.append(total)
            day += 1

        self.assertEqual(expected, points)

    # test function with empty month (previous month)
    def test_get_month_points_empty_month(self):
        points = get_month_points(self.user, self.month-1)

        expected = []

        # create a range of days of the month in that year
        days = calendar.monthrange(year=self.year, month=self.month-1)[1]
        day = 1
        while day <= days:
            expected.append(0)
            day += 1

        self.assertEqual(expected, points)

    # test function with empty user
    def test_get_month_points_invalid_user(self):
        points = get_month_points(None, self.month)

        expected = []

        # create a range of days of the month in that year
        days = calendar.monthrange(year=self.year, month=self.month)[1]
        day = 1
        while day <= days:
            expected.append(0)
            day += 1

        self.assertEqual(expected, points)

class BuildProfilesTests(TestCase):

    def setUp(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        self.user = User.objects.create_user(username=username,password=password)
        self.up = UserProfile.objects.create(user=self.user)

        username2 = 'mary'
        password2 = 'sue123'

        self.user2 = User.objects.create_user(username=username2,password=password2)
        self.up2 = UserProfile.objects.create(user=self.user2)

    # test function with accepted status
    def build_profiles_accepted_ok(self):
        rel = Relation.objects.create(primary_user=self.user,secondary_user=self.user2,status=RelStatus.ACCEPTED)
        up = build_profiles(user=self.user,relationships=rel)
        expected = [self.up2]
        self.assertEqual(expected, up)

    # test function when user sends user 2 a request
    def build_profiles_pending_ok(self):
        rel = Relation.objects.create(primary_user=self.user,secondary_user=self.user2,status=RelStatus.REQUESTED)
        up = build_profiles(user=self.user,relationships=rel)
        expected = [self.up2]
        self.assertEqual(expected, up)

    # test function when user 2 sends user  request
    def build_profiles_request_ok(self):
        rel = Relation.objects.create(primary_user=self.user2,secondary_user=self.user,status=RelStatus.REQUESTED)
        up = build_profiles(user=self.user,relationships=rel)
        expected = [self.up2]
        self.assertEqual(expected, up)

    # test function when users are invalid or empty
    def build_profiles_invalid_fail(self):
        up = build_profiles(user=None,relationships=None)
        expected = []
        self.assertEqual(expected, up)



# Forms test

class LoginFormTests(TestCase):

    # test login form ok credentials
    def test_login_form_ok(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        User.objects.create_user(username=username,password=password)

        # create form data
        test_data = {'username': username, 'password': password}
        form = LoginForm(data=test_data)

        self.assertTrue(form.is_valid())

    # test login with empty credentials
    def test_login_form_empty(self):
        # empty credentials
        username = ''
        password = ''

        # create form data
        test_data =  {'username':username, 'password': password}
        form = LoginForm(data=test_data)

        self.assertFalse(form.is_valid())

    # test login with invalid credentials
    def test_login_form_invalid(self):
        # Create dummy data
        username = 'idonot'
        password = 'exist'

        # create form data
        test_data = {'username': username, 'password': password}
        form = LoginForm(data=test_data)

        self.assertFalse(form.is_valid())
        self.assertRaisesMessage(forms.ValidationError, "Incorrect username or password")

    # test login form with inactive account
    def test_login_form_inactive(self):
        # Create dummy data
        username = 'iamnot'
        password = 'active'

        user = User.objects.create_user(username=username,password=password)
        user.is_active = False
        user.save()

        # create form data
        test_data = {'username': username, 'password': password}
        form = LoginForm(data=test_data)

        self.assertFalse(form.is_valid())
        self.assertRaisesMessage(forms.ValidationError, "Account has been disabled")

    # test login form login function
    def test_login_form_login(self):
        # Create dummy data
        username = 'john'
        password = 'smith123'

        user = User.objects.create_user(username=username,password=password)

        # create form data
        test_data = {'username': username, 'password': password}
        form = LoginForm(data=test_data)

        self.assertTrue(form.is_valid())
        self.assertEqual(user, form.login())

class RegisterFormTests(TestCase):

    # test register form with valid input
    def test_register_form_ok(self):
        # create form data
        username = 'john'
        email = 'john@smith.com'
        password = 'smith123'

        test_data = {'username': username,
                     'email': email,
                     'email2': email,
                     'password': password,
                     'password2': password,}

        form = RegisterForm(data=test_data)

        self.assertTrue(form.is_valid())

    # test register form failure when user tries to register a duplicate user
    def test_register_form_user_exists(self):
        # Create dummy data
        username = 'john'
        email = 'john@smith.com'
        password = 'smith123'

        user = User.objects.create_user(username=username,password=password,email=email)

        # create form data : only username remains the same
        email = 'valid@email.com'
        password = 'password123'

        test_data = {'username': username,
                     'email': email,
                     'email2': email,
                     'password': password,
                     'password2': password,}

        form = RegisterForm(data=test_data)

        self.assertFalse(form.is_valid())
        self.assertRaisesMessage(forms.ValidationError, "Username already exists")

    # test register form failure when user tries to register with invalid username
    def test_register_form_username_invalid(self):
        # create form data
        username = 'j'
        email = 'john@smith.com'
        password = 'smith123'

        test_data = {'username': username,
                     'email': email,
                     'email2': email,
                     'password': password,
                     'password2': password,}

        form = RegisterForm(data=test_data)

        self.assertFalse(form.is_valid())
        self.assertRaisesMessage(forms.ValidationError, "Username must be at least 3 characters")

    # test register form failure when user tries to register a duplicate email
    def test_register_form_email_exists(self):
        # Create dummy data
        username = 'john'
        email = 'john@smith.com'
        password = 'smith123'

        user = User.objects.create_user(username=username,password=password,email=email)

        # create form data: only email remains the same
        username = 'user'
        password = 'password123'
        test_data = {'username': username,
                     'email': email,
                     'email2': email,
                     'password': password,
                     'password2': password,}

        form = RegisterForm(data=test_data)

        self.assertFalse(form.is_valid())
        self.assertRaisesMessage(forms.ValidationError, "This email is already in use")

    # test register form failure when user tries to register an invalid email
    def test_register_form_invalid_email(self):
        # create form data
        username = 'john'
        email = 'johnsmithcom'
        password = 'smith123'

        test_data = {'username': username,
                     'email': email,
                     'email2': email,
                     'password': password,
                     'password2': password,}

        form = RegisterForm(data=test_data)

        self.assertFalse(form.is_valid())

    # test register form failure when user tries to register an invalid confirmation email
    def test_register_form_invalid_email2(self):
        # create form data
        username = 'john'
        email = 'john@smith.com'
        email2 = 'mary@sue.com'
        password = 'smith123'

        test_data = {'username': username,
                     'email': email,
                     'email2': email2,
                     'password': password,
                     'password2': password,}

        form = RegisterForm(data=test_data)

        self.assertFalse(form.is_valid())
        self.assertRaisesMessage(forms.ValidationError, "Confirmation email does not match")

    # test register form failure when user tries to register an invalid password
    def test_register_form_invalid_password(self):
        # create form data
        username = 'john'
        email = 'john@smith.com'
        password = '123'

        test_data = {'username': username,
                     'email': email,
                     'email2': email,
                     'password': password,
                     'password2': password,}

        form = RegisterForm(data=test_data)

        self.assertFalse(form.is_valid())
        self.assertRaisesMessage(forms.ValidationError, "Password must be at least 6 characters")

    # test register form failure when user tries to register an invalid confirmation password
    def test_register_form_invalid_password2(self):
        # create form data
        username = 'john'
        email = 'john@smith.com'
        password = 'smith123'
        password2 = 'sue123'

        test_data = {'username': username,
                     'email': email,
                     'email2': email,
                     'password': password,
                     'password2': password2,}

        form = RegisterForm(data=test_data)

        self.assertFalse(form.is_valid())
        self.assertRaisesMessage(forms.ValidationError, "Confirmation password does not match")

    # test register form failure when user tries to register an incomplete form
    def test_register_form_incomplete(self):
        # create form data
        username = 'john'
        email = 'john@smith.com'
        password = 'smith123'

        test_data = {'username': username,
                     'email2': email,
                     'password2': password,}

        form = RegisterForm(data=test_data)

        self.assertFalse(form.is_valid())

class DailyFormTests(TestCase):

    def test_daily_form_ok(self):
        action1 = 'action1'
        action2 = 'action2'
        action3 = 'action3'

        test_data = {'action1': action1,
                     'action2': action2,
                     'action3': action3}

        form =  DailyForm(data=test_data)

        self.assertTrue(form.is_valid())


class GoalFormTests(TestCase):

    def test_goal_form_ok(self):
        goal = 'my goal'

        test_data = {'goal': goal}
        form = GoalForm(data=test_data)

        self.assertTrue(form.is_valid())


class DescFormTests(TestCase):

    def test_desc_form_ok(self):
        description = 'description of the user'

        test_data = {'description' : description}
        form = DescForm(data=test_data)

        self.assertTrue(form.is_valid())

class PasswordFormTests(TestCase):

    def setUp(self):
        # create dummy
        username = 'john'
        email = 'john@smith.com'
        password = 'smith123'

        user = User.objects.create_user(username=username,password=password,email=email)
        self.factory = RequestFactory()
        self.request = self.factory.get('')
        self.request.user = user

    # test valid input on password form
    def test_password_form_ok(self):
        # create form data
        old_password = 'smith123'
        password = 'smithereens123'

        test_data = {'old_password': old_password,
                     'password': password,
                     'password2': password }

        form = PasswordForm(data=test_data,request=self.request)

        self.assertTrue(form.is_valid())

    # test that password form with invalid old password fails
    def test_password_form_invalid_old_password(self):
        # create form data
        old_password = 'wrongpassword1234'
        password = 'doesntmatter'

        test_data = {'old_password': old_password,
                     'password': password,
                     'password2': password }

        form = PasswordForm(data=test_data,request=self.request)

        self.assertFalse(form.is_valid())
        self.assertRaisesMessage(forms.ValidationError, "Password is invalid")

    # test that password form with invalid confirmation password fails
    def test_password_form_invalid_password2(self):
        # create form data
        old_password = 'smith123'
        password = 'somethingnew'
        password2 = 'somethingthatisntthefirstpassword'

        test_data = {'old_password': old_password,
                     'password': password,
                     'password2': password2 }

        form = PasswordForm(data=test_data,request=self.request)

        self.assertFalse(form.is_valid())
        self.assertRaisesMessage(forms.ValidationError, "Confirmation password does not match")

    # test that password form with invalid new password fails
    def test_password_form_invalid_password(self):
        # create form data
        old_password = 'smith123'
        password = '123'

        test_data = {'old_password': old_password,
                     'password': password }

        form = PasswordForm(data=test_data,request=self.request)

        self.assertFalse(form.is_valid())
        self.assertRaisesMessage(forms.ValidationError, "Password must be at least 6 characters")

    # test password form change password function
    def test_password_form_change_password(self):
        # create form data
        old_password = 'smith123'
        password = 'smithereens123'

        test_data = {'old_password': old_password,
                     'password': password,
                     'password2': password }

        form = PasswordForm(data=test_data,request=self.request)

        self.assertTrue(form.is_valid())
        self.assertEqual(password, form.change_password())

class EmailFormTests(TestCase):

    def setUp(self):
        # create dummy
        username = 'john'
        email = 'john@smith.com'
        password = 'smith123'

        user = User.objects.create_user(username=username,password=password,email=email)
        self.factory = RequestFactory()
        self.request = self.factory.get('')
        self.request.user = user

    # test valid input on email form
    def test_email_form_ok(self):
        # create form data
        email = 'smith@smithy.com'

        test_data = {'email': email,
                     'email2': email,}

        form = EmailForm(data=test_data,request=self.request)

        self.assertTrue(form.is_valid())

    # test new email fails if it exists in DB
    def test_email_form_email_exists(self):
        # create form data
        email = 'john@smith.com'

        test_data = {'email': email,
                     'email2': email,}

        form = EmailForm(data=test_data,request=self.request)

        self.assertFalse(form.is_valid())
        self.assertRaisesMessage(forms.ValidationError, "This email is already in use")

    # test invalid email2 input
    def test_email_form_invalid_email2(self):
        # create form data
        email = 'smith@smithy.com'
        email2 = 'not@thesame.com'

        test_data = {'email': email,
                     'email2': email2,}

        form = EmailForm(data=test_data,request=self.request)

        self.assertFalse(form.is_valid())
        self.assertRaisesMessage(forms.ValidationError, "Confirmation email does not match")

    # test invalid email input
    def test_email_form_invalid_email(self):
        # create form data
        email = 'smithsmithycom'

        test_data = {'email': email,
                     'email2': email,}

        form = EmailForm(data=test_data,request=self.request)

        self.assertFalse(form.is_valid())

class MessageFormTests(TestCase):

    def test_message_form_ok(self):
        message= 'A message for you'

        test_data = {'message' : message}
        form = MessageForm(data=test_data)

        self.assertTrue(form.is_valid())





