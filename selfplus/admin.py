from django.contrib import admin
from selfplus.models import *

class DailyLogAdmin(admin.ModelAdmin):
    list_display = ('user','daily','completed')

class DailyAdmin(admin.ModelAdmin):
    list_display = ('user', 'action', 'active')

class RelationAdmin(admin.ModelAdmin):
    list_display = ('primary_user', 'secondary_user', 'status')

# Register your models here.
admin.site.register(Daily, DailyAdmin)
admin.site.register(DailyLog,DailyLogAdmin)
admin.site.register(Relation,RelationAdmin)
admin.site.register(UserProfile)
admin.site.register(Message)