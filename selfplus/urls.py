from django.conf.urls import patterns, url
from selfplus import views

urlpatterns = patterns(
    '',
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.user_login, name='user_login'),
    url(r'^register/$', views.register, name='register'),
    url(r'^recovery/$', views.recovery, name='recovery'),
    url(r'^recovery/change/(?P<id>[^/]+)/$', views.recovery_change, name='recovery_change'),
    url(r'^setup-profile/$', views.setup_profile, name='setup_profile'),
    url(r'^user/dailies/$', views.user_page, name='user_page'),
    url(r'^user/dailies/complete/$', views.dailies_complete, name='dailies_complete'),
    url(r'^user/profile/$', views.user_profile, name='user_profile'),
    url(r'^user/add-desc/$', views.add_desc, name='add_desc'),
    url(r'^user/add-daily/$', views.add_daily, name='add_daily'),
    url(r'^user/add-goal/$', views.add_goal, name='add_goal'),
    url(r'^user/progress/$', views.user_progress, name='user_progress'),
    url(r'^user/friends/$', views.user_friends, name='user_friends'),
    url(r'^user/settings/$', views.user_settings, name='user_settings'),
    url(r'^user/settings/change-password/$', views.change_password, name='change_password'),
    url(r'^user/settings/change-email/$', views.change_email, name='change_email'),
    url(r'^user/settings/edit-profile/$', views.edit_profile, name='edit_profile'),
    url(r'^community/$', views.community, name='community'),
    url(r'^community/search/$', views.community_search, name='community_search'),
    url(r'^community/(?P<username>\w+)/$', views.community_user, name='community_user'),
    url(r'^community/(?P<username>\w+)/add-message/$', views.add_message, name='add_message'),
    url(r'^community/(?P<username>\w+)/add-friend/$', views.add_friend, name='add_friend'),
    url(r'^community/(?P<username>\w+)/cancel-request/$', views.cancel_request, name='cancel_request'),
    url(r'^community/(?P<username>\w+)/accept/$', views.accept_friend, name='accept_friend'),
    url(r'^community/(?P<username>\w+)/reject/$', views.reject_friend, name='reject_friend'),
    url(r'^logout/$', views.user_logout, name='logout'),

)


