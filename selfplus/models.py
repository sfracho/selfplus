from django.db import models
from django.contrib.auth.models import User

# python imports
import uuid

# Helpers
# Callable for changing filename
def content_file_name(instance, filename):
    return 'profile_images/user_{0}/{1}'.format(instance.user.id, filename)

# Enumerate friendship status for relation model
class RelStatus:
    REQUESTED = 1
    ACCEPTED = 2
    BLOCKED = 3

# User Profile
class UserProfile(models.Model):
    # Links UserProfile to a User model instance.
    user = models.OneToOneField(User)

    # Additional attributes
    # User profile picture
    picture = models.ImageField(upload_to=content_file_name, blank=True)
    # User thumbnail picture
    thumbnail = models.ImageField(upload_to=content_file_name, blank=True)
    # User total points
    points = models.PositiveIntegerField(default=0)
    # User goal
    goal = models.CharField(max_length=128)
    # Personal description
    description = models.TextField(max_length=512)
    # Privacy control
    is_private =  models.BooleanField(default=True)
    # Setup complete
    is_setup = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.points < 0:
            self.points = 0
        super(UserProfile, self).save(*args, **kwargs)

    # Override the __unicode__() method
    def __unicode__(self):
        return self.user.username

# Daily Goal
class Daily(models.Model):
    # Description of the daily action
    action = models.CharField(max_length=128)
    # Point associated with the daily
    points = models.PositiveSmallIntegerField(default=0)
    # User who started this daily
    user = models.ForeignKey(User)
    # Date this goal was created
    date_created = models.DateField(auto_now=True)
    # Active/Inactive
    # Can't delete entries here entirely, daily log depends on it
    active = models.BooleanField(default=True)

    # Override the __unicode__() method
    def __unicode__(self):
        return self.action

# Daily Goal Log
class DailyLog(models.Model):
    # Completed by
    user = models.ForeignKey(User)
    # Daily log completed
    daily = models.ForeignKey(Daily)
    # When this daily was completed
    completed = models.DateField()

    # Override the __unicode__() method
    def __unicode__(self):
        return self.user.username

# Messages
class Message(models.Model):
    # Message Text
    text = models.TextField(max_length=128)
    # Message to
    receiver = models.ForeignKey(User,related_name="message_received")
    # Messsage from
    sender = models.ForeignKey(User,related_name="message_sent")
    # Timestamp
    date_posted = models.DateTimeField(auto_now=True)

    # Override the __unicode__() method
    def __unicode__(self):
        return self.text

class Relation(models.Model):
    # The user who initiated the relationship invite
    primary_user = models.ForeignKey(User,related_name="relation_requested")
    # The user who received the relationship invite
    secondary_user = models.ForeignKey(User,related_name="relation_accepted")
    # The date this relationship was formed
    date_formed = models.DateTimeField(auto_now_add=True)
    # Status of the relationship
    status = models.PositiveSmallIntegerField(default=RelStatus.REQUESTED)

    class Meta:
        unique_together = ('primary_user','secondary_user')

    # Override the __unicode__() method
    def __unicode__(self):
        return self.primary_user.username

class PasswordChangeRequests(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User)
    timestamp = models.DateTimeField(auto_now_add=True)
    is_expired = models.BooleanField(default=False)

