# Django imports
from django import forms
from django.contrib.auth import authenticate
from django.core.files.images import get_image_dimensions
from django.conf import settings
from django.core.files.uploadedfile import InMemoryUploadedFile

# Project imports
from selfplus.models import *

# Dependency imports
from PIL import Image

# Python imports
import StringIO


class LoginForm(forms.Form):
    username = forms.CharField(max_length=255)
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = self.cleaned_data
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        if not user:
            raise forms.ValidationError("Incorrect username or password")
        if not user.is_active:
            raise forms.ValidationError("Account has been disabled")
        return self.cleaned_data

    def login(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        return user

class RegisterForm(forms.Form):
    username = forms.CharField(max_length=128, help_text="Username:")
    email = forms.EmailField(help_text="Email:")
    email2 = forms.EmailField(help_text="Confirm Email:")
    password = forms.CharField(widget=forms.PasswordInput, help_text="Password:")
    password2 = forms.CharField(widget=forms.PasswordInput, help_text="Confirm Password:")

    def clean(self):
        cleaned_data = self.cleaned_data
        username = cleaned_data.get('username')
        email = cleaned_data.get('email')
        email2 = cleaned_data.get('email2')
        password = cleaned_data.get('password')
        password2 = cleaned_data.get('password2')

        user_exists = User.objects.filter(username=username)
        email_exists = User.objects.filter(email=email)
        if user_exists:
            print "user exists"
            raise forms.ValidationError({'username':["Username already exists"]})
        if username and len(username) <3:
            print "username length too small"
            raise forms.ValidationError({'username':["Username must be at least 3 characters"]})
        if email_exists:
            print "email exists"
            raise forms.ValidationError({'email':["This email is already in use"]})
        if email != email2:
            print "email does not match"
            raise forms.ValidationError({'email2':["Confirmation email does not match"]})
        if password and len(password) <6:
            print "password length too small"
            raise forms.ValidationError({'password':["Password must be at least 6 characters"]})
        if password != password2:
            print "password does not match"
            raise forms.ValidationError({'password2':["Confirmation password does not match"]})
        return cleaned_data


class DailyForm(forms.Form):
    action1 = forms.CharField(max_length=128, help_text="Priority 1:", required=False)
    action2 = forms.CharField(max_length=128, help_text="Priority 2:", required=False)
    action3 = forms.CharField(max_length=128, help_text="Priority 3:", required=False)

class GoalForm(forms.Form):
    goal = forms.CharField(max_length=128, help_text="What's your goal?", required=False)

class DescForm(forms.Form):
    description = forms.CharField(max_length=512, widget=forms.Textarea, help_text="Tell us about yourself:")

class ProfileForm(forms.Form):
    picture = forms.ImageField(required=False, help_text="Upload Image:")

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(ProfileForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        picture = cleaned_data.get('picture')

        if picture:
            w, h = get_image_dimensions(picture)
            if not picture.content_type in settings.VALID_IMAGE_FORMATS:
                print "incorrect format"
                raise forms.ValidationError({'picture':["Only *.jpg and *.png images are allowed."]})
            if w != settings.PROFILE_DIMENSION_W  or h != settings.PROFILE_DIMENSION_H :
                print "incorrect image dimension"
                raise forms.ValidationError({'picture':["Image dimension must be 128x128"]})
        return cleaned_data

    def change_picture(self):
        request = self.request
        img = request.FILES['picture']
        up = UserProfile.objects.get(user=request.user)
        up.picture = img
        up.thumbnail = create_thumbnail(img.name, up.picture)
        up.save()

    def clear_picture(self):
        request = self.request
        up = UserProfile.objects.get(user=request.user)
        up.picture = 'profile_images/default.jpg'
        up.thumbnail = 'profile_images/default_thumbnail.jpg'
        up.save()

class PasswordForm(forms.Form):
    old_password = forms.CharField(widget=forms.PasswordInput, help_text="Old Password:")
    password = forms.CharField(widget=forms.PasswordInput, help_text="New Password:")
    password2 = forms.CharField(widget=forms.PasswordInput, help_text="Confirm Password:")

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(PasswordForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        old_password = cleaned_data.get('old_password')
        password = cleaned_data.get('password')
        password2 = cleaned_data.get('password2')

        password_check = authenticate(username=self.request.user.username,password=old_password)

        if not password_check:
            print "password is incorrect"
            raise forms.ValidationError({'old_password':["Password is invalid"]})
        if len(password) <6:
            print "password length too small"
            raise forms.ValidationError({'password':["Password must be at least 6 characters"]})
        if password != password2:
            print "password does not match"
            raise forms.ValidationError({'password2':["Confirmation password does not match"]})

        return cleaned_data

    def change_password(self):
        user = self.request.user
        password = self.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        return password

class RecoverPasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput, help_text="New Password:")
    password2 = forms.CharField(widget=forms.PasswordInput, help_text="Confirm Password:")

    def clean(self):
        cleaned_data = self.cleaned_data
        password = cleaned_data.get('password')
        password2 = cleaned_data.get('password2')

        if password != password2:
            print "password does not match"
            raise forms.ValidationError({'password2':["Confirmation password does not match"]})

        return cleaned_data

    def change_password(self, user):
        user = user
        password = self.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        return password


class EmailForm(forms.Form):
    email = forms.EmailField(help_text="New Email:")
    email2 = forms.EmailField(help_text="Confirm Email:")

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(EmailForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        email = cleaned_data.get('email')
        email2 = cleaned_data.get('email2')

        email_exists = User.objects.filter(email=email)
        if email_exists:
            print "email exists"
            raise forms.ValidationError({'email':["This email is already in use"]})
        if email != email2:
            print "email does not match"
            raise forms.ValidationError({'email2':["Confirmation email does not match"]})
        return cleaned_data

    def change_email(self):
        user = self.request.user
        email = self.cleaned_data.get('email')
        user.email = email
        user.save()

class MessageForm(forms.Form):
    message = forms.CharField(max_length=128, help_text="Post a message:",widget=forms.Textarea)

class RecoveryForm(forms.Form):
    email = forms.EmailField(help_text="Email: ")

    # verify if email has a match
    def match(self):
        email = self.cleaned_data.get('email')
        link_id = None

        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            user = None

        if user and user.is_active:
            link = PasswordChangeRequests.objects.create(user=user)
            link_id = link.id

        return email, link_id


# ----------------------------------------------------------------------------------------------------------------------
# HELPERS
# ----------------------------------------------------------------------------------------------------------------------
# Helper functions to avoid repetition
# Creating thumbnails
def create_thumbnail(filename, img_file):

    size = 64, 64

    try:
        im = Image.open(img_file)
        im.thumbnail(size, Image.ANTIALIAS)
        thumb_io = StringIO.StringIO()
        im.save(thumb_io, format="JPEG")

        thumb_file = InMemoryUploadedFile(thumb_io, None, filename, "img/jpeg", thumb_io.len, None)

    except IOError:
        print "cannot create thumbnail for '%s'" % img_file

    return thumb_file