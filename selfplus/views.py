# Django Imports
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import login, logout
from django.db.models import Sum
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.core.mail import send_mail

# Project Imports
from selfplus.forms import *
from selfplus.search import *

# python imports
import calendar
import datetime


def index(request):

    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('user_page'))

    # Render forms
    # Login form
    login_form = LoginForm(request.POST or None)
    register_form = RegisterForm(request.POST or None)

    context_dict = {}
    context_dict['login_form'] = login_form
    context_dict['register_form'] = register_form

    return render(request, 'selfplus/index.html', context_dict)

def user_login(request):
    # Login form
    login_form = LoginForm(request.POST or None)

    if request.method == 'POST':
        if login_form.is_valid():
            user = login_form.login()
            if user:
                login(request,user)
                up = UserProfile.objects.get(user=user)
                if up.is_setup:
                    return HttpResponseRedirect(reverse('user_page'))
                else:
                    return HttpResponseRedirect(reverse('setup_profile'))

    context_dict = {}
    context_dict['login_form'] = login_form
    return render(request, 'selfplus/login.html', context_dict)

# This function is disabled for now because project settings
# will contain sensitive information if it is enabled
def recovery(request):
    # Login form
    recovery_form = RecoveryForm(request.POST or None)

    if request.method == 'POST':
        if recovery_form.is_valid():
            email, link = recovery_form.match()
            if email and link:
                subject = 'Password Recovery'
                address = request.build_absolute_uri() + "change/" + str(link)
                message = 'Click the link to recover your password: \n' + address
                sender = ''
                recipient = email
                send_mail(subject, message, sender, [recipient], fail_silently=False)

        messages.success(request, 'Email has been sent to the user.')
        HttpResponseRedirect(reverse('recovery'))

    context_dict = {}
    context_dict['recovery_form'] = recovery_form
    return render(request, 'selfplus/recovery.html', context_dict)

# This function is disabled for now because project settings
# will contain sensitive information if it is enabled
def recovery_change(request, id):

    # Retrieve Forms
    recover_password_form = RecoverPasswordForm(request.POST or None)

    # Link
    try:
        recovery_request = PasswordChangeRequests.objects.get(id=id)
    except PasswordChangeRequests.DoesNotExist:
        recovery_request = None
    except ValueError:
        recovery_request = None

    if not recovery_request:
        return HttpResponseRedirect(reverse('index'))

    is_expired = (datetime.datetime.utcnow() - recovery_request.timestamp.replace(tzinfo=None)) > datetime.timedelta(1)

    if is_expired or recovery_request.is_expired:
        # Set database item to expire if had not
        recovery_request.is_expired = True
        recovery_request.save()
        messages.error(request,'This page has expired.')

    if request.method == 'POST':
        if recover_password_form.is_valid():
            recover_password_form.change_password(user=recovery_request.user)
            messages.success(request, 'Password successfully changed')
            # Set database item to expire
            recovery_request.is_expired = True
            recovery_request.save()

    context_dict = {}
    context_dict['recover_password_form'] = recover_password_form
    context_dict['link'] = reverse('recovery_change', args=[id])
    return render(request,'selfplus/recovery_change.html',context_dict)

def register(request):
    # Render forms
    register_form = RegisterForm(request.POST or None)
    login_form = LoginForm(request.POST or None)

    if request.method == 'POST':
        if register_form.is_valid():
            # Get post information
            username = request.POST['username']
            email = request.POST['email']
            password = request.POST['password']

            # Save to the model
            user = User.objects.create_user(username=username)
            user.set_password(password)
            user.email = email
            user.save()

            # create corresponding profile
            up = UserProfile.objects.create(user=user)
            up.picture = 'profile_images/default.jpg'
            up.thumbnail = 'profile_images/default_thumbnail.jpg'
            up.save()

            # Successful registration page
            user = authenticate(username=username, password=password)
            login(request,user)
            return HttpResponseRedirect(reverse('setup_profile'))
        else:
            context_dict = {}
            context_dict['login_form'] = login_form
            context_dict['register_form'] = register_form
            return render(request, 'selfplus/index.html', context_dict)

    return HttpResponseRedirect(reverse('index'))

@login_required(redirect_field_name=None)
def setup_profile(request):

    daily_form = DailyForm(request.POST)
    goal_form = GoalForm(request.POST)

    up = UserProfile.objects.get(user=request.user)

    if request.method == 'POST':
        if daily_form.is_valid() and goal_form.is_valid():
            up.goal = request.POST['goal']
            up.is_setup = True
            up.save()
            add_daily(request)

    if up.is_setup:
        return HttpResponseRedirect(reverse('user_page'))

    # Add pass to get
    context_dict = {}
    context_dict['daily_form'] = daily_form
    context_dict['goal_form'] = goal_form
    return render(request, 'selfplus/setup_profile.html', context_dict)

@login_required(redirect_field_name=None)
def user_profile(request):

    # Get UserProfile
    up = get_profile(request.user)

    # Dailies
    dailies = []
    get_dailies(request.user,dailies)

    # Get messages
    messages,page_range,current_page = get_messages(request=request,user=request.user)
    profile_list = UserProfile.objects.filter(user__message_sent=messages.object_list).order_by('-user__message_sent__date_posted')
    profiled_msgs = zip(messages,profile_list)

    # Render forms
    # Description form
    desc_form = DescForm(initial={'description':up.description})

    context_dict = {}
    context_dict['desc_form'] = desc_form
    context_dict['messages'] = profiled_msgs
    context_dict['page_range'] = page_range
    context_dict['current_page'] = current_page
    context_dict['userprofile'] = up
    context_dict['username'] = request.user.username
    context_dict['daily1'] = dailies[0]
    context_dict['daily2'] = dailies[1]
    context_dict['daily3'] = dailies[2]

    # Is user the owner of the page? Used in conjunction with viewing other people's profile
    # We are reusing the same html file for guesting
    # see: def community_user view
    context_dict['owner'] = True

    # Navbar Toggle - css aesthetics only
    navbar_toggle(context_dict=context_dict, navbar_id='profile_nav')

    return render(request, 'selfplus/userprofile.html', context_dict)

@login_required(redirect_field_name=None)
def add_desc(request):

    form = DescForm(request.POST)

    if request.method == 'POST':
        if form.is_valid():
            # Retrieve goal value and store into UserProfile object
            up = UserProfile.objects.get(user=request.user)
            up.description = request.POST['description']
            up.save()

    return HttpResponseRedirect(reverse('user_profile'))

@login_required(redirect_field_name=None)
def user_page(request):
    # Calendar functions -- move later to its own APP
    # Get current date
    now = datetime.datetime.now()
    # Create calendar instance
    c = calendar.Calendar()
    # Set Calendar date to Sunday
    c.setfirstweekday(calendar.SUNDAY)

    monthly_calendar = c.monthdatescalendar(now.year, now.month)

    # Get UserProfile
    up = get_profile(request.user)

    # Get DailyLogs of the month
    dlog = DailyLog.objects.filter(user=request.user).filter(completed__range=(monthly_calendar[0][0],monthly_calendar[-1][-1]))

    # Reconstruct calendar with daily log information
    full_calendar = fill_calendar(calendar=monthly_calendar, dlog=dlog)

    # Get Daily Actions
    daily_actions = []
    get_dailies(request.user, daily_actions)

    # Get Dailies
    dailies = []
    get_dailies(request.user, dailies,action_only=False)

    # Get Today's log
    active_logs = get_active_logs(dailies=dailies,date=now)

    # Render forms
    daily_form = DailyForm(initial={'action1':daily_actions[0],'action2':daily_actions[1],'action3':daily_actions[2]})
    goal_form = GoalForm(initial={'goal':up.goal})

    # Add pass to get
    context_dict = {}
    context_dict['daily_form'] = daily_form
    context_dict['goal_form'] = goal_form
    context_dict['calendar'] = full_calendar
    context_dict['month'] = now.month
    context_dict['monthname'] = now.strftime("%B")
    context_dict['username'] = request.user.username
    context_dict['userprofile'] = up
    context_dict['daily1'] = daily_actions[0]
    context_dict['daily2'] = daily_actions[1]
    context_dict['daily3'] = daily_actions[2]
    context_dict['daily1_complete'] = active_logs[0] is not None
    context_dict['daily2_complete'] = active_logs[1] is not None
    context_dict['daily3_complete'] = active_logs[2] is not None

    # Navbar Toggle - css aesthetics only
    navbar_toggle(context_dict=context_dict, navbar_id='dailies_nav')

    return render(request, 'selfplus/userpage.html', context_dict)

@login_required(redirect_field_name=None)
def dailies_complete(request):
    # Get date today
    now = datetime.datetime.now()

    # Get Dailies
    dailies = []
    get_dailies(request.user,dailies,action_only=False)

    # Get UserProfile for scoring
    up = UserProfile.objects.get(user=request.user)

    daily_id = request.GET.get('daily_id')

    if daily_id == "1":
        log = DailyLog.objects.create(user=request.user, daily=dailies[0], completed=now)
        log.save()
        up.points += log.daily.points
        up.save()
    elif daily_id == "2":
        log = DailyLog.objects.create(user=request.user, daily=dailies[1], completed=now)
        log.save()
        up.points += log.daily.points
        up.save()
    elif daily_id == "3":
        log = DailyLog.objects.create(user=request.user, daily=dailies[2], completed=now)
        log.save()
        up.points += log.daily.points
        up.save()

    return HttpResponseRedirect(reverse('user_page'))

@login_required(redirect_field_name=None)
def add_daily(request):

    form = DailyForm(request.POST)
    if request.method == 'POST':
        if form.is_valid():

            actions = []

            # Get the values of the form
            actions.append(request.POST.get('action1', None))
            actions.append(request.POST.get('action2', None))
            actions.append(request.POST.get('action3', None))

            # Filter out empty string
            actions = filter(None,actions)

            points = [15, 10, 5]
            priority = 0

            for action in actions:
                try:
                    active = Daily.objects.get(user=request.user,points=points[priority],active=True)
                except Daily.DoesNotExist:
                    active = None

                # if user's entry exists and it was changed
                if active and active.action != action:
                    # deactivate it
                    active.active = False
                    active.save()
                    # create a new active daily
                    daily = Daily.objects.create(action=action,points=points[priority],user=request.user, date_created=datetime.datetime.now())
                    daily.save()
                # if user has no entry yet, create it
                if not active:
                    daily = Daily.objects.create(action=action,points=points[priority],user=request.user, date_created=datetime.datetime.now())
                    daily.save()

                priority += 1

            # deactivate trailing entries
            while priority < 3:
                try:
                    active = Daily.objects.get(user=request.user,points=points[priority],active=True)
                except Daily.DoesNotExist:
                    active = None

                if active:
                    active.active = False
                    active.save()

                priority += 1

    return HttpResponseRedirect(reverse('user_page'))

@login_required(redirect_field_name=None)
def add_goal(request):

    form = GoalForm(request.POST)

    if request.method == 'POST':
        if form.is_valid():
            # Retrieve goal value and store into UserProfile object
            up = UserProfile.objects.get(user=request.user)
            up.goal = request.POST['goal']
            up.save()

    else:
        print form.errors

    return HttpResponseRedirect(reverse('user_page'))

@login_required(redirect_field_name=None)
def community(request):
    public_users = User.objects.filter(userprofile__is_private=False)
    tscorers = public_users.order_by('-userprofile__points')[:5]
    tscorers_profiles = UserProfile.objects.filter(user=tscorers).order_by('-points')
    newcomers = public_users.order_by('-date_joined')[:5]
    newcomers_profiles = UserProfile.objects.filter(user=newcomers).order_by('-user__date_joined')

    context_dict = {}
    context_dict['tscorers'] = zip(tscorers, tscorers_profiles)
    context_dict['newcomers'] = zip(newcomers, newcomers_profiles)
    context_dict['search_return'] = False

    # Navbar Toggle - css aesthetics only
    navbar_toggle(context_dict=context_dict, navbar_id='community_nav')

    return render(request, 'selfplus/community.html', context_dict)

@login_required(redirect_field_name=None)
def community_search(request):

    search_str = request.GET.get('str')

    context_dict = {}
    if search_str:

        public_users = User.objects.filter(userprofile__is_private=False)

        entry_query = get_query(search_str,['username'])
        users_names = public_users.filter(entry_query)
        ups_names = UserProfile.objects.filter(user=users_names)
        results_names = zip(users_names,ups_names)

        entry_query = get_query(search_str,['action'])
        daily = Daily.objects.filter(entry_query).filter(active=True)
        users_dailies = public_users.filter(daily=daily)
        ups_dailies = UserProfile.objects.filter(user=users_dailies)
        results_dailies = zip(users_dailies,ups_dailies)

        context_dict['search_return'] = True
        context_dict['search_str'] = search_str
        context_dict['results_names'] = results_names
        context_dict['results_dailies'] = results_dailies

    else:
        return HttpResponse()

    return render(request, 'selfplus/community_search.html', context_dict)

@login_required(redirect_field_name=None)
def community_user(request, username):
    # Perform check if viewer selects himself from the community
    if(request.user.username == username):
        return HttpResponseRedirect(reverse('user_profile'))

    try:
        # Get username of community member
        u = User.objects.get(username=username)
    except:
        return HttpResponseRedirect(reverse('community'))

    # Get UserProfile of community member
    up = get_profile(u)

    if up.is_private:
        return HttpResponseRedirect(reverse('community'))

    # Dailies of community member
    dailies = []
    get_dailies(u,dailies=dailies)

    # Get messages of community member
    messages, page_range, current_page = get_messages(request=request,user=u)
    profile_list = UserProfile.objects.filter(user__message_sent=messages.object_list).order_by('-user__message_sent__date_posted')
    profiled_msgs = zip(messages, profile_list)

    # What is the status of the user's relationship with this community member?
    # Did the user send an invite to the member?
    try:
        rel_primary = Relation.objects.get(primary_user=request.user,secondary_user=u)
    except:
        rel_primary = None

    # Did the member send the user an invite?
    try:
        rel_secondary = Relation.objects.get(primary_user=u,secondary_user=request.user)
    except:
        rel_secondary = None

    # Render Forms
    message_form = MessageForm()

    context_dict = {}
    context_dict['message_form'] = message_form
    context_dict['messages'] = profiled_msgs
    context_dict['page_range'] = page_range
    context_dict['current_page'] = current_page
    context_dict['username'] = username
    context_dict['userprofile'] = up
    context_dict['daily1'] = dailies[0]
    context_dict['daily2'] = dailies[1]
    context_dict['daily3'] = dailies[2]
    context_dict['rel_primary'] = rel_primary
    context_dict['rel_secondary'] = rel_secondary

    # Is user the owner of the page? Used in conjunction with viewing other people's profile
    # We are reusing the same html file for guesting
    # see: def user_profile view
    context_dict['owner'] = False

    # Navbar Toggle - css aesthetics only
    navbar_toggle(context_dict=context_dict, navbar_id='community_nav')

    return render(request, 'selfplus/userprofile.html', context_dict)

@login_required(redirect_field_name=None)
def add_message(request, username):

    form = MessageForm(request.POST)

    if request.method == 'POST':
        if form.is_valid():
            # Retrieve message value and store it
            text = request.POST['message']
            receiver = User.objects.get(username=username)
            sender = request.user
            message = Message.objects.create(text=text,receiver=receiver,sender=sender)
            message.save()

    return HttpResponseRedirect(reverse('community_user',args=[username]))

@login_required(redirect_field_name=None)
def add_friend(request, username):

    visitor = request.user
    owner = User.objects.get(username=username)

    # first search if a relationship exists
    # The user has sent an invite
    try:
        sender = Relation.objects.get(primary_user=visitor,secondary_user=owner)
    except:
        sender = None
    # The member has sent an invite
    try:
        receiver = Relation.objects.get(primary_user=owner,secondary_user=visitor)
    except:
        receiver = None

    if receiver:
        # The member has already sent a request so adding him is mutual
        receiver.status = RelStatus.ACCEPTED
        receiver.save()
    elif not sender :
        # This entry is new, so create a relationship of the nature of a friend request
        friend_request = Relation.objects.create(primary_user=visitor,secondary_user=owner,status=RelStatus.REQUESTED)
        friend_request.save()

    return HttpResponseRedirect(reverse('community_user', args=[username]))

@login_required(redirect_field_name=None)
def accept_friend(request,username):

    visitor = request.user
    owner = User.objects.get(username=username)

    # Verify the member's invite
    try:
        receiver = Relation.objects.get(primary_user=owner,secondary_user=visitor)
    except:
        receiver = None

    if receiver:
        receiver.status = RelStatus.ACCEPTED
        receiver.save()

    return HttpResponseRedirect(reverse('user_friends'))

@login_required(redirect_field_name=None)
def reject_friend(request,username):

    visitor = request.user
    owner = User.objects.get(username=username)

    # Verify the member's invite
    try:
        receiver = Relation.objects.get(primary_user=owner,secondary_user=visitor)
    except:
        receiver = None

    if receiver:
        receiver.delete()

    return HttpResponseRedirect('/selfplus/user/friends/')

@login_required(redirect_field_name=None)
def cancel_request(request,username):

    visitor = request.user
    owner = User.objects.get(username=username)

    # The user has sent an invite
    try:
        sender = Relation.objects.get(primary_user=visitor,secondary_user=owner)
    except:
        sender = None

    if sender:
        sender.delete()

    return HttpResponseRedirect('/selfplus/user/friends/')

@login_required(redirect_field_name=None)
def user_progress(request):

    current_date = datetime.datetime.now()
    # Get current year to display in the chart
    year = current_date.year
    # Get current month to display in the chart
    month = current_date.month
    # Get selectable year list
    year_list = range( request.user.date_joined.year,year+1 )
    # Get selectable month list
    month_list = range(1,month+1)
    # Make this list into month names
    month_list = [ calendar.month_abbr[m] for m in month_list ]

    context_dict = {}
    # Navbar Toggle - css aesthetics only
    navbar_toggle(context_dict=context_dict, navbar_id='progress_nav')

    if request.is_ajax():
        request_year = request.GET.get('year')
        request_month = request.GET.get('month')

        if(request_year):
            # Get all logs within the year
            year_points = get_year_points(user=request.user,year=request_year)
            context_dict['year_points'] = year_points
        if(request_month):
            # Get all logs within the month
            month_points = get_month_points(user=request.user,month=request_month)
            context_dict['month_points'] = month_points
            month_days = calendar.monthrange(year=year, month=int(request_month))[1]
            context_dict['month_days'] = month_days
        return JsonResponse(context_dict)

    month_days = calendar.monthrange(year=year, month=month)[1]
    year_points = get_year_points(user=request.user,year=year)
    month_points = get_month_points(user=request.user,month=month)
    context_dict['current_year'] = year
    context_dict['current_month'] = month
    context_dict['month_days'] = month_days
    context_dict['year_points'] = year_points
    context_dict['month_points'] = month_points
    context_dict['year_list'] = year_list
    context_dict['month_list'] = month_list

    return render(request,'selfplus/progress.html',context_dict)

@login_required(redirect_field_name=None)
def user_friends(request):

    # Make a preliminary query before checking status

    # Case where user sent an invite
    sent_filter = Relation.objects.filter(primary_user=request.user)
    # Case when user was invited
    received_filter = Relation.objects.filter(secondary_user=request.user)

    # Make further query filters depending on usage

    # For the pending list in which user has made a request
    pending_list = sent_filter.filter(status=RelStatus.REQUESTED)
    pending_profiles = build_profiles(user=request.user,relationships=pending_list)

    # For the request list for which the user is invited
    request_list = received_filter.filter(status=RelStatus.REQUESTED)
    request_profiles = build_profiles(user=request.user,relationships=request_list)

    # For the friend list
    friend_list1 = sent_filter.filter(status=RelStatus.ACCEPTED)
    list1_profiles = build_profiles(user=request.user, relationships=friend_list1)
    friend_list2 = received_filter.filter(status=RelStatus.ACCEPTED)
    list2_profiles = build_profiles(user=request.user, relationships=friend_list2)
    # The friend list
    friend_profiles = list1_profiles + list2_profiles

    context_dict = {}
    context_dict['pending'] = pending_profiles
    context_dict['requests'] = request_profiles
    context_dict['friends'] = friend_profiles

    # Navbar Toggle - css aesthetics only
    navbar_toggle(context_dict=context_dict, navbar_id='friends_nav')

    return render(request,'selfplus/friends.html',context_dict)

@login_required(redirect_field_name=None)
def user_settings(request):

    # Retrieve Profile
    up = UserProfile.objects.get(user=request.user)
    is_private = up.is_private

    # Change privacy settings
    if request.is_ajax() and request.method == 'POST':
        if request.POST['private_box'] == "False":
            up.is_private = False
            is_private = False
            up.save()
        else:
            up.is_private = True
            is_private = True
            up.save()

    # Retrieve Forms
    password_form = PasswordForm(request=request)
    email_form = EmailForm(request=request)
    profile_form = ProfileForm(request=request)

    context_dict = {}
    context_dict['userprofile'] = up
    context_dict['profile_form'] = profile_form
    context_dict['password_form'] = password_form
    context_dict['email_form'] = email_form
    context_dict['is_private'] = is_private

    return render(request,'selfplus/settings.html',context_dict)

@login_required(redirect_field_name=None)
def edit_profile(request):

    # Retrieve Forms
    password_form = PasswordForm(request=request)
    email_form = EmailForm(request=request)
    profile_form = ProfileForm(request.POST, request.FILES,request=request)

    if request.method == 'POST':
        if 'default' in request.POST:
            profile_form.clear_picture()
            messages.success(request, 'Profile picture reverted to default.')
        elif profile_form.is_valid():
            if 'picture' in request.FILES:
                profile_form.change_picture()
                messages.success(request, 'Profile picture updated.')

        # Retrieve Profile
        up = UserProfile.objects.get(user=request.user)

        context_dict = {}
        context_dict['userprofile'] = up
        context_dict['profile_form'] = profile_form
        context_dict['password_form'] = password_form
        context_dict['email_form'] = email_form
        return render(request,'selfplus/settings.html',context_dict)

    return HttpResponseRedirect(reverse('user_settings'))

@login_required(redirect_field_name=None)
def change_password(request):
    # Retrieve Forms
    password_form = PasswordForm(request.POST or None,request=request)
    email_form = EmailForm(request=request)
    profile_form = ProfileForm(request=request)

    # Retrieve Profile
    up = UserProfile.objects.get(user=request.user)

    if request.method == 'POST':
        if password_form.is_valid():
            password = password_form.change_password()
            username = request.user.username
            logout(request)
            user = authenticate(username=username,password=password)
            if user:
                login(request,user)
                messages.success(request, 'Password successfully changed')
                return HttpResponseRedirect('/selfplus/user/settings/')

        context_dict = {}
        context_dict['userprofile'] = up
        context_dict['profile_form'] = profile_form
        context_dict['password_form'] = password_form
        context_dict['email_form'] = email_form
        return render(request,'selfplus/settings.html',context_dict)

    return HttpResponseRedirect(reverse('user_settings'))

@login_required(redirect_field_name=None)
def change_email(request):
    # Retrieve Forms
    password_form = PasswordForm(request=request)
    email_form = EmailForm(request.POST or None,request=request)
    profile_form = ProfileForm(request=request)

    # Retrieve Profile
    up = UserProfile.objects.get(user=request.user)

    if request.method == 'POST':
        if email_form.is_valid():
            email_form.change_email()
            messages.success(request, 'Email successfully changed')
            return HttpResponseRedirect(reverse('user_settings'))

        context_dict = {}
        context_dict['userprofile'] = up
        context_dict['profile_form'] = profile_form
        context_dict['password_form'] = password_form
        context_dict['email_form'] = email_form
        return render(request,'selfplus/settings.html',context_dict)

    return HttpResponseRedirect(reverse('user_settings'))

@login_required(redirect_field_name=None)
def user_logout(request):
    # logout user
    logout(request)
    return HttpResponseRedirect(reverse('index'))

# ----------------------------------------------------------------------------------------------------------------------
# HELPERS
# ----------------------------------------------------------------------------------------------------------------------
# Helper functions to avoid repetition
# Getting dailies

def get_dailies(user, dailies, action_only=True):

    try:
        daily1 = Daily.objects.get(user=user,points=15,active=True)
        if(action_only):
            dailies.append(daily1.action)
        else:
            dailies.append(daily1)
    except Daily.DoesNotExist:
        dailies.append(None)
    try:
        daily2 = Daily.objects.get(user=user,points=10,active=True)
        if(action_only):
            dailies.append(daily2.action)
        else:
            dailies.append(daily2)
    except Daily.DoesNotExist:
        dailies.append(None)
    try:
        daily3 = Daily.objects.get(user=user,points=5,active=True)
        if(action_only):
            dailies.append(daily3.action)
        else:
            dailies.append(daily3)
    except Daily.DoesNotExist:
        dailies.append(None)

    return

# Getting user profile
def get_profile(user):
    try:
        up = UserProfile.objects.get(user=user)
    except UserProfile.DoesNotExist:
        up = None

    return up

# Navbar toggle functionality (aesthetics only)
def navbar_toggle(context_dict, navbar_id):
    context_dict['profile_nav'] = 'inactive'
    context_dict['dailies_nav'] = 'inactive'
    context_dict['progress_nav'] = 'inactive'
    context_dict['friends_nav'] = 'inactive'
    context_dict['community_nav'] = 'inactive'

    # set  a default value just incase navbar_id content is invalid
    if navbar_id in context_dict:
        context_dict[navbar_id] = 'active'
    else:
        context_dict['profile_nav'] = 'active'

    return

#Get messages
def get_messages(request, user):
    message_list = Message.objects.filter(receiver=user).order_by('-date_posted')
    profile_list = UserProfile.objects.filter()
    paginator = Paginator(message_list,5)

    page = request.GET.get('page')

    # Create pages for the messsages
    try:
        messages = paginator.page(page)
    except PageNotAnInteger:
        messages = paginator.page(1)
    except EmptyPage:
        print paginator.num_pages
        messages = paginator.page(paginator.num_pages)

    # Turn current page into an integer
    try:
        current_page = int(page)
    except TypeError:
        current_page = 1

    return messages, paginator.page_range, current_page

# Add daily log details to the calendar
def fill_calendar(calendar, dlog):
    full_calendar = []

    for week in calendar:
        full_week = []
        for date in week:
            logs = dlog.filter(completed=date)
            total_points = 0
            if logs:
                for log in logs:
                    total_points = total_points + log.daily.points
            full_week.append((date,logs,total_points))
        full_calendar.append(full_week)

    return full_calendar

# Get logs from dlogs at a certain date
def get_active_logs(dailies,date):
    logs = []

    try:
        log = DailyLog.objects.get(daily=dailies[0],completed=date)
        logs.append(log)
    except DailyLog.DoesNotExist:
        logs.append(None)

    try:
        log = DailyLog.objects.get(daily=dailies[1],completed=date)
        logs.append(log)
    except DailyLog.DoesNotExist:
        logs.append(None)

    try:
        log = DailyLog.objects.get(daily=dailies[2],completed=date)
        logs.append(log)
    except DailyLog.DoesNotExist:
        logs.append(None)

    return logs

# Get point list per month over a year
def get_year_points(user,year):

    now = datetime.datetime.now()

    year_points = []
    yearly_logs = DailyLog.objects.filter(user=user,completed__year=year)
    first_day = datetime.date(day=1,month=1,year=int(year))
    prev_points = DailyLog.objects.filter(user=user,completed__lt=first_day).aggregate(points_total=Sum('daily__points'))

    month_points = prev_points['points_total'] or 0
    month_limit = 12
    if now.year == int(year):
            month_limit = now.month
    month = 1
    while month <= month_limit:
        month_log_points = yearly_logs.filter(completed__month=month).aggregate(month_points_total=Sum('daily__points'))
        month_points_add = month_log_points['month_points_total'] or 0
        month_points = month_points + month_points_add
        year_points.append(month_points)
        month += 1
    return year_points

# Get point list per day over a month
def get_month_points(user,month):

    now = datetime.datetime.now()

    month_points = []
    month_logs = DailyLog.objects.filter(user=user,completed__year=now.year,completed__month=month)
    first_day = datetime.date(day=1,month=int(month),year=now.year)
    prev_points = DailyLog.objects.filter(user=user,completed__lt=first_day).aggregate(points_total=Sum('daily__points'))

    day_points = prev_points['points_total'] or 0
    days = calendar.monthrange(year=now.year, month=int(month))[1]
    if now.month == int(month):
        days = now.day
    day = 1
    while day <= days:
        logs = month_logs.filter(completed__day=day)
        for log in logs:
            day_points = day_points + log.daily.points
        month_points.append(day_points)
        day += 1

    return month_points

# Create user profiles from relationship queries
def build_profiles(user, relationships):
    profiles = []
    for rel in relationships:
        requester = rel.primary_user
        receiver = rel.secondary_user
        if requester != user:
            up = UserProfile.objects.get(user=requester)
            profiles.append((requester, up))
        else:
            up = UserProfile.objects.get(user=receiver)
            profiles.append((receiver,up))
    return profiles