import os
import csv

def populate():

    with open('test_data.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            username = row['username']
            email = row['email']
            password = row['password']
            action1 = row['action1']
            action2 = row['action2']
            action3 = row['action3']
            goal = row['goal']
            desc = row['desc']
            record = row['record']
            motivation = row['motivation']

            user = add_user(username=username,email=email,password=password)
            add_dailies(user=user,action1=action1,action2=action2, action3=action3)
            add_goals(user=user,goal=goal)
            add_desc(user=user,desc=desc)
            add_log(user=user,year=int(record),motivation=int(motivation))

            print "added new user: " + username

    csvfile.closed

def add_user(username,email,password):
    # create user
    user = User.objects.create_user(username=username,email=email)
    user.set_password(password)
    user.save()

    # create corresponding profile
    up = UserProfile.objects.create(user=user)
    up.is_private = False
    up.picture = 'profile_images/default.jpg'
    up.thumbnail = 'profile_images/default_thumbnail.jpg'
    up.is_setup = True
    up.save()

    return user

def add_superuser(username, password):
    # create user
    user = User.objects.create_superuser(username=username,email="",password="")
    user.set_password(password)
    user.save()

    # create corresponding profile
    up = UserProfile.objects.create(user=user)
    up.picture = 'profile_images/default.jpg'
    up.thumbnail = 'profile_images/default_thumbnail.jpg'
    up.save()

def add_dailies(user,action1="",action2="",action3=""):
    # Create the daily objects
    daily1 = Daily.objects.create(action=action1,points=15,user=user, date_created = datetime.datetime.now() )
    daily2 = Daily.objects.create(action=action2,points=10,user=user, date_created = datetime.datetime.now() )
    daily3 = Daily.objects.create(action=action3,points=5,user=user, date_created = datetime.datetime.now() )

    # Store objects in the database
    daily1.save()
    daily2.save()
    daily3.save()

def add_goals(user,goal=""):
    up = UserProfile.objects.get(user=user)
    up.goal = goal
    up.save()

def add_desc(user,desc=""):
    up = UserProfile.objects.get(user=user)
    up.description = desc
    up.save()

def add_points(user,points=0):
    up = UserProfile.objects.get(user=user)
    up.points = points
    up.save()

def add_log(user, year, motivation):
    dailies = list(Daily.objects.filter(user=user))
    now = datetime.datetime.now()
    up = UserProfile.objects.get(user=user)

    month = 1
    month_limit = 12
    while month <= month_limit:
        days = calendar.monthrange(year,month)[1]
        if now.year == year and now.month == month:
            month_limit = month
            days = now.day
        day = 1
        while day <= days:
            completed = date(year=year,month=month,day=day)
            # Randomize a number from 0 to 9
            # a hit is an instant completed daily of 3. people with high motivations have a
            # high chance of scoring a 3
            hit = random.randint(0,9)
            if hit<motivation:
                n = 3
            else:
                # if we didn't get a hit, randomize the lower numbers
                n = random.randint(0,2)
            # Shuffle Dailies
            random.shuffle(dailies)
            # Create the logs
            for daily in dailies[0:n]:
                log = DailyLog.objects.create(user=user,daily=daily,completed=completed)
                log.save()
                up.points += log.daily.points
                up.save()
            day += 1
        month += 1

def add_random_log(user):
    dailies = Daily.objects.filter(user=user)

# Start execution here!
if __name__ == '__main__':
    print "Starting Selfplus population script..."
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'self_plus_project.settings')
    import django
    import datetime
    import random
    import calendar
    from datetime import date, timedelta
    django.setup()
    from selfplus.models import *
    User.objects.all().delete()
    UserProfile.objects.all().delete()
    Daily.objects.all().delete()
    DailyLog.objects.all().delete()
    add_superuser(username="sam",password="sam")
    populate()