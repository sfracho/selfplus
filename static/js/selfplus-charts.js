
var year_points = [];
var month_points = [];
var month_days = 0;
var my_rows;

function create_year_chart(ypoints) {
  year_points.length = 0;
  year_points = ypoints;
}

function create_month_chart(mpoints, mdays) {
  month_points.length = 0;
  month_points = mpoints;
  month_days = mdays;
}

google.load('visualization', '1', {packages: ['corechart', 'line']});
google.setOnLoadCallback(drawBasicYear);
google.setOnLoadCallback(drawBasicMonth);

function drawBasicYear() {
  // year chart
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Month');
  data.addColumn('number', 'Total Points');

  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var rows = [];
  for(i=0; i<=months.length; i++){
	if(i<year_points.length){
		rows.push([months[i],year_points[i]]);
	}
	else{
		rows.push([months[i],null]);
	}
  }
  data.addRows(rows);

  var options = {
	hAxis: {
	  title: 'Month'
	},
	vAxis: {
	  title: 'Points'
	},
	interpolateNulls: true
  };

  var year_chart = new google.visualization.LineChart(document.getElementById('year_chart'));
  year_chart.draw(data, options);
}

function drawBasicMonth() {
  // month chart
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Day');
  data.addColumn('number', 'Total Points');

  var rows = [];
  for(i=0; i<month_days; i++){
	 if(i<month_points.length){
	  rows.push([i+1+"",month_points[i]]);
	 }
	 else
	 {
	  rows.push([i+1+"",null]);
	 }
  }
  data.addRows(rows);

  var options = {
	hAxis: {
	  title: 'Day'
	},
	vAxis: {
	  title: 'Points'
	},
	interpolateNulls: true
  };

  var month_chart = new google.visualization.LineChart(document.getElementById('month_chart'));
  month_chart.draw(data, options);
}
