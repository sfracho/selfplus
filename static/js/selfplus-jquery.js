function show(divId) {
    $("#" + divId).removeClass('hidden');
}
function hide(divId) {
    $("#" + divId).addClass('hidden');
}

var submitted = false;
$("form").submit(function(){
    if(!submitted){
        submitted = true;
        return true;
    }
    else
    {
        return false;
    }
});