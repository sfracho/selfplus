// Cookie setup
function getCookie(name)
{
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?

            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$.ajaxSetup({
     beforeSend: function(xhr, settings) {
         if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
             // Only send the token to relative URLs i.e. locally.
             xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
         }
     }
});

// Other functions

$('#yearly_select').change(function(){
    var year_val;
    year_val = $(this).val();
    $.getJSON('/selfplus/user/progress/', {year: year_val}, function(data){
        array = data.year_points;
        create_year_chart(array);
        drawBasicYear();
    });
});

$('#monthly_select').change(function(){
    var month_val;
    month_val = $(this).val();
    $.getJSON('/selfplus/user/progress/', {month: month_val}, function(data){
        array = data.month_points;
        month_days = data.month_days
        create_month_chart(array, month_days);
        drawBasicMonth();
    });
});

$('#community_search').bind('submit',function(){
    var search_str;
    search_str = $('#search_bar').val();
    $.get('/selfplus/community/search/', {str: search_str}, function(data){
        if(data){
            $('#search_results').html(data);
        }
    });
    return false;
});

$('#community_search_btn').bind('click',function(){
    var search_str;
    search_str = $('#search_bar').val();
    $.get('/selfplus/community/search/', {str: search_str}, function(data){
        if(data){
            $('#search_results').html(data)
        }
    });
    return false;
});

$('#id_private_box').change( function(){
    var is_checked;
    if($("#id_private_box").is(':checked'))
        is_checked = "True"
    else
        is_checked = "False"
    $.post('/selfplus/user/settings/', {private_box: is_checked} , function(data){
    });
    return false;
});