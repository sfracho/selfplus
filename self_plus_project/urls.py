from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.conf import settings

# site starts at selfplus folder
urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^selfplus/', include('selfplus.urls')),
]

# media is uploaded here on debug mode
if settings.DEBUG:
    urlpatterns += patterns(
        'django.views.static',
        (r'^media/(?P<path>.*)','serve',
        {'document_root': settings.MEDIA_ROOT}), )

